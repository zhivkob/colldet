//
//  ColUtilsGLES.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ColUtilsGLES.h"
#import "Mushroom.h"
#import "Cage.h"
#import "Crate.h"
#import "Table.h"
#import "Vase.h"
#import "BarChair.h"
#import "GolfBall.h"

@implementation ColUtilsGLES

static ColUtilsGLES *sharedUtils = nil;

#pragma mark -
#pragma mark Initialization
#pragma mark -

/** @name Initialization */

/**
 *  We override this method to avoid thread issues with the singleton instance of the class.
 *
 */

+ (void)initialize
{
    if (self == [ColUtilsGLES class]) {
        sharedUtils = [[ColUtilsGLES alloc] init];
    }
}

/**
 *  @return sharedUtils Returns an allocated and initialized instance of the class.
 *
 */

+ (id)sharedUtils
{
    return sharedUtils;
}

#pragma mark -
#pragma mark Geometry Loading And Drawing
#pragma mark -


/** @name Handling geometry */

/**
 *  Use this method to load a custom geometry. The current release copies the data - once it puts each _vertex, normal and/or texture_ in separate vectors and once in an interleaved array for rendering.
 *  
 *  @param geo A pointer to the geometry of the mesh.
 *  @param geometryType The geometry type of the _.obj_ object.
 *
 *  @return Returns a _NO_ value if the method succeeded an _YES_ otherwise.
 */

- (BOOL)loadGeometry:(col::ColGeometry *)geo withGeometryType:(ColGeometryType)geometryType
{
    geo->clearOldData();
    
    switch(geometryType) {
        case ColGeometryTypeCrate:
            [self fillGeometry:geo withVerticesArray:CrateVerts verticesCount:CrateNumVerts];
            break;
        case ColGeometryTypeCage:
            [self fillGeometry:geo withVerticesArray:CageVerts verticesCount:CageNumVerts];
            break;
        case ColGeometryTypeBarChair:
            [self fillGeometry:geo withVerticesArray:BarChairVerts verticesCount:BarChairNumVerts];
            break;
        case ColGeometryTypeBall:
            [self fillGeometry:geo withVerticesArray:GolfBallVerts verticesCount:GolfBallNumVerts];
            break;
        case ColGeometryTypeTable:
            [self fillGeometry:geo withVerticesArray:TableVerts verticesCount:TableNumVerts];
            break;
        case ColGeometryTypeVase:
            [self fillGeometry:geo withVerticesArray:VaseVerts verticesCount:VaseNumVerts];
            break;
        case ColGeometryTypeMushroom:
            [self fillGeometry:geo withVerticesArray:MushroomVerts verticesCount:MushroomNumVerts];
            break;
        case ColGeometryTypeOther:
            break;
        default:
            break;
    }
    
    geo->setNormalType(geo->getPoints().size() == geo->getNormals().size() ? col::PER_VERTEX : col::PER_PRIMITIVE);
    geo->setPrimType( col::ANY );
    geo->setPrimType( col::TRIANGLES );
    geo->updateBBox();
    
    return NO;
}

/**
 *  This is a convenience method for filling the geometry data using an array containing the vertices, normals and texture coordinates of an object and the number of faces it has.
 *
 *  @param geo A pointer to the geometry of the mesh.
 *  @param array A reference to the array containg the interleaved data.
 *  @param numVerts The number of faces of the object.
 *
 *  @return Returns a _NO_ value if the method succeeded an _YES_ otherwise.
 */

- (void)fillGeometry:(col::ColGeometry *)geo withVerticesArray:(float [])array verticesCount:(unsigned int)numVerts
{
    // Get a reference to the vertices, normals and indices
    col::Pnt3Array& vertices = *geo->editPointsPtr();
    col::Vec3Array& normals = *geo->editNormalsPtr();
    col::PrimArray& indices = *geo->editPrimitivesPtr();
    vertices.resize(numVerts);
    normals.resize(numVerts);
    indices.resize(numVerts / 3);
    
    unsigned int stride = 8;
    for (int i = 0; i < numVerts; i++) {
        col::Point3f vertex;
        col::Vector3f normal;
        
        for (int j = 0; j < 3; j++) {
            vertex[j] = array[i * stride + j];
            normal[j] = array[i * stride + 3 + j];
        }
        vertices[i] = vertex;
        normals[i] = normal;
    }
    for (int i = 0; i < numVerts / 3; i++) {
        col::Primitive face;
        face.push_back(i * 3);
        face.push_back(i * 3 + 1);
        face.push_back(i * 3 + 2);
        indices[i] = face;
    }
}

/**
 *  A method that draws a given geometry to the screen using the OpenGL command *glDrawElements*. It takes the size of the primitives vector
 *  of the geometry and gives it to the OpenGL method. The type of the variables is *GL_UNSIGNED_SHORT* (2 bytes). We assume that a
 *  vertex array containing the address to the indices is already specified, so we use 0 as a memory address for the indices here.
 *
 *  @param geo A pointer to the geometry of the mesh.
 *  @param mode The mode we will use to draw the geometry, i.e. the mode OpenGL will use to interpret the data (e.g. *GL_TRIANGLES*, *GL_LINES*, etc.)
 *  @param geometryType The geometry type of the object to be drawn.
 *
 */

- (void)drawGeometry:(col::ColGeometry *)geo withMode:(GLenum)mode geometryType:(ColGeometryType)geometryType
{
    if (geometryType != ColGeometryTypeOther) {
        switch(geometryType) {
            case ColGeometryTypeCrate:
                glDrawArrays(mode, 0, CrateNumVerts);
                break;
            case ColGeometryTypeCage:
                glDrawArrays(mode, 0, CageNumVerts);
                break;
            case ColGeometryTypeBarChair:
                glDrawArrays(mode, 0, BarChairNumVerts);
                break;
            case ColGeometryTypeBall:
                glDrawArrays(mode, 0, GolfBallNumVerts);
                break;
            case ColGeometryTypeTable:
                glDrawArrays(mode, 0, TableNumVerts);
                break;
            case ColGeometryTypeVase:
                glDrawArrays(mode, 0, VaseNumVerts);
                break;
            case ColGeometryTypeMushroom:
                glDrawArrays(mode, 0, MushroomNumVerts);
                break;
            case ColGeometryTypeOther:
                
                break;
            default:
                break;
        }
    }
    else {
        // Get a constant reference to the points, primitives and normals of the geometry
        col::Pnt3Array vertices = geo->getPoints();
        col::Vec3Array normals = geo->getNormals();
        col::PrimArray indices = geo->getPrimitives();
        
        // Load OBJ Data
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 0, &vertices[0]);
        glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 0, &normals[0]);
//        glDrawArrays(mode, 0, 10);
    }
}

#pragma mark -
#pragma mark Geometry Creation
#pragma mark -

/**
 *  There may be redundant faces, i.e., faces with _face_nv[i] = 0, 1, 2_.
 *
 *  + _normals_ has _fCount_ many normals.
 *  + _face[i]_ must have _Dop::NumOri_ many columns!
 *  + The variable _NumOri_ is still used.
 *
 *  @exception XColBug if a face has more than NumOri many vertices.
 *
 *  @param geo A pointer to the mesh geometry.
 *  @param vertex Vertex array (in).
 *  @param vCount Vertex array size (in).
 *  @param face Faces array (in).
 *  @param faceNv Faces normals array (in).
 *  @param fCount Faces array size (in).
 *
 */

- (void)buildGeometry:(col::ColGeometry *)geo
           fromPoints:(const Vectormath::Aos::Point3 [])vertex
            withCount:(unsigned int)vCount
                faces:(const unsigned int [])face
           andNormals:(const unsigned int [])faceNv
            withCount:(unsigned int)fCount
{
    geo->clearOldData();
    
    for ( unsigned int i = 0; i < fCount; i ++ )
        if ( faceNv[i] > col::Dop::NumOri )
            throw col::XCollision("geomFromPoints: a face has more than NumOri vertices");
    
    vector<col::TopoFace> face_vec( fCount );
    
    unsigned int offset = 0;
    face_vec[0].set(face, faceNv[0]);
    for ( unsigned int i = 1; i < fCount; i ++ )
    {
        offset = offset + faceNv[i-1];
        face_vec[i].set( face + offset, faceNv[i] );
    }
    
    col::Pnt3Array vertex_vec( vCount );
    for ( unsigned int i = 0; i < vCount; i ++ )
        vertex_vec[i] = vertex[i];
    
    [self buildGeometry:geo fromPointsArray:vertex_vec andFaces:face_vec];
}

/**
 *
 *  Create a polyhedron which is the object described by the planes given by _normals, nfaces, vertex, face, face_nv_. Actually, normals is only used for sorting the vertices of the faces. If _normals == NULL_, then we assume that vertices in _face[]_ are already sorted in counter-clockwise order. _face_ contains indices into vertex.
 *
 *  + There may be redundant faces, i.e., faces with _face[i].size() = 0, 1, 2_.
 *  + The geometry will have no material.
 *
 *  If _normals != NULL_, then _face_ will get sorted!
 *
 *  @exception XColBug If there are no faces with any vertices.
 *
 *  Planes are given by _Ori*x - d = 0_.
 *  _normals_ has _face.size()_ many normals.
 *
 *  @param geo The mesh geometry.
 *  @param vertex The vertex array (in).
 *  @param face Faces array	(in, could get sorted).
 *
 */

- (void)buildGeometry:(col::ColGeometry *)geo
      fromPointsArray:(const col::Pnt3Array &)vertex
             andFaces:(std::vector<col::TopoFace> &)face
{
    geo->clearOldData();
    
    // points
    geo->editPointsPtr()->resize( vertex.size() );
    for ( unsigned int i = 0; i < vertex.size(); i ++ )
        (*geo->editPointsPtr())[i] = vertex[i];
    
    // geometry/primitives
    for ( unsigned int i = 0; i < face.size(); i ++ )
    {
        // polygon
        col::Primitive prim( face[i].size() );
        for ( unsigned int j = 0; j < face[i].size(); j ++ )
        {
            if ( face[i][j] >= vertex.size() )
                throw col::XCollision("buildGeometryFromPoints: index face[%d][%d] = %d >= num. vertices = %d", i,j, face[i][j], vertex.size() );
            
            prim[j] = ( face[i][j] );
        }
        
        geo->editPrimitivesPtr()->push_back( prim );
    }
    
    [self addNormalsToGeometry:geo];
}

/**
 *  This method makes a sphere geometry.
 *
 *  @param geo Holds a reference to the mesh geometry.
 *  @param depth The depth of the sphere, i.e. the smoothness of the sphere (optimal between _1-3_).
 *  @param radius The radius of the sphere.
 */

- (void)makeSphere:(col::ColGeometry *)geo
         withDepth:(unsigned int)depth
         andRadius:(REAL)radius
{
    geo->clearOldData();
    
    const REAL HALF_PI = M_PI / 2.0f;
    
    // Rotate
    Vectormath::Aos::Matrix4 mat = Vectormath::Aos::Matrix4::rotation( acos( IcosahedronZ ) + HALF_PI, Vectormath::Aos::Vector3(0.0,1.0,0.0) );
    
    // Initiate the sphere from icosahedron
    col::Pnt3Array points;
    col::Vec3Array norms;
    for ( unsigned int i=0; i<12; i++ )
    {
        Vectormath::Aos::Point3 p3( IcosahedronPoints[i] );
        Vectormath::Aos::Vector4 v4 = mat * p3;
        Vectormath::Aos::Vector3 v3( v4[0], v4[1], v4[2] );
        
        // Care! normalize( v3 ) =!= normalize( v4 )
        Vectormath::Aos::Vector3 norm = normalize( v3 );
        norms.push_back( norm );
        
        Vectormath::Aos::Vector3 vnr = norm * radius;
        Vectormath::Aos::Point3 pt( vnr[0], vnr[1], vnr[2] );
        points.push_back( pt );
    }
    
    std::vector<col::Primitive> tris;
    
    // Recursive subdivision
    unsigned int len = 12;
    for ( unsigned int i=0; i<20; i++ )
        [self subdivideTriangleWithIndices:IcosahedronIndices[ i*3+0 ] index2:IcosahedronIndices[ i*3+1 ] index3:IcosahedronIndices[ i*3+2 ] depth:depth radius:radius length:&len pointsArray:&points normalsArray:&norms andTriangles:&tris];
    
    *( geo->editPointsPtr() ) = points;
    *( geo->editPrimitivesPtr() ) = tris;
    *( geo->editNormalsPtr() ) = norms;

    geo->updateBBox();
}

/** @name Composition of geometry */

/**
 *  This method makes a cube geometry.
 *
 *  @param geo Holds a reference to the mesh geometry.
 *  @param radius The half length of the edge of the cube.
 */

- (void)makeCube:(col::ColGeometry *)geo withRadius:(REAL)radius
{
    geo->clearOldData();
    
    Vectormath::Aos::Point3 pcube[8];
    for ( unsigned int i=0; i<8; i++ )
    {
        pcube[i] = Vectormath::Aos::Point3( CubePoints[i][0] * radius,
                                           CubePoints[i][1] * radius,
                                           CubePoints[i][2] * radius );
    }
    
    unsigned int face_nv[12] = { 3,3, 3,3, 3,3, 3,3, 3,3, 3,3 };
    [self buildGeometry:geo fromPoints:pcube withCount:8 faces:CubeTriIndices andNormals:face_nv withCount:12];
    
    geo->updateBBox();
}

/**
 *  This method makes a plane triangle mesh.
 *
 *  @param geo Holds a reference to the mesh geometry.
 *  @param width The horizontal length of x-axis.
 *  @param height The vertical length of y-axis.
 *  @param hor The horizontal grid size.
 *  @param vert The vertical grid size.
 */

- (void)makePlane:(col::ColGeometry *)geo
        withWidth:(REAL)width
           height:(REAL)height
 atVerticalHeight:(unsigned int)vert
andHorizontalHeight:(unsigned int)hor
{
    if ( !vert || !hor )
    {
        NSLog(@"%s : %s", __FILE__, __FUNCTION__ );
        NSLog(@"vert and hor must be greater than 0");
        NSLog(@"Now vert = %d, hor = %d", vert, hor);
        return;
    }
    geo->clearOldData();
    
    REAL xstep = width / hor;
    REAL ystep = height / vert;
    
    // points
    for ( unsigned int y = 0; y <= hor;  y++ )
        for ( unsigned int x = 0; x <= vert; x++ )
        {
            geo->editPointsPtr()->push_back( Vectormath::Aos::Point3( x * xstep - width / 2, y * ystep - height / 2, 0 ) );
        }
    // primitives
    for ( unsigned int y = 0; y < hor;  y++ )
        for ( unsigned int x = 0; x < vert; x++ )
        {
            col::Primitive p1(3);
            p1[0] = x + (y  )*(vert+1);
            p1[1] = x + (y  )*(vert+1) + 1;
            p1[2] = x + (y+1)*(vert+1) + 1;
            geo->editPrimitivesPtr()->push_back( p1 );
            
            col::Primitive p2(3);
            p2[0] = x + (y  )*(vert+1);
            p2[1] = x + (y+1)*(vert+1) + 1;
            p2[2] = x + (y+1)*(vert+1);
            geo->editPrimitivesPtr()->push_back( p2 );
        }
    
    [self addNormalsToGeometry:geo];
    
    // type
    geo->setPrimType( col::TRIANGLES );
    
    geo->updateBBox();
}

#pragma mark -
#pragma mark Geometry Creation Helper Methods
#pragma mark -

/**
 *  The intermediate function called recursively by the makeSphere:withDepth:andRadius: method.
 *
 *  @param i1 The first index of the triangle.
 *  @param i2 The second index of the triangle.
 *  @param i3 The third index of the triangle.
 *  @param depth The depth of the sphere.
 *  @param radius The radius of the sphere.
 *  @param len The points count of a icosahedron.
 *  @param points A pointer to the points of the sphere.
 *  @param norms A pointer to the normals of the sphere.
 *  @param tris A pointer to all the faces of the sphere.
 */

- (void)subdivideTriangleWithIndices:(unsigned int)i1
                              index2:(unsigned int)i2
                              index3:(unsigned int)i3
                               depth:(unsigned int)depth
                              radius:(REAL)radius
                              length:(unsigned int *)len
                         pointsArray:(col::Pnt3Array *)points
                        normalsArray:(col::Vec3Array *)norms
                        andTriangles:(std::vector<col::Primitive> *)tris
{
    if ( depth == 0 )
    {
        col::Primitive tri;
        tri.push_back( i1 );
        tri.push_back( i2 );
        tri.push_back( i3 );
        tris->push_back( tri );
        
        return;
    }
    
    Vectormath::Aos::Point3 v1 = (*points)[i1];
    Vectormath::Aos::Point3 v2 = (*points)[i2];
    Vectormath::Aos::Point3 v3 = (*points)[i3];
    Vectormath::Aos::Point3 v12, v23, v31;
    
    v12 = v1 + (v2 - v1) * .5f;
    v23 = v2 + (v3 - v2) * .5f;
    v31 = v3 + (v1 - v3) * .5f;
    
    unsigned short i12 = (*len)++;
    unsigned short i23 = (*len)++;
    unsigned short i31 = (*len)++;
    
    [self addPoint:v12 withIndex:i12 radius:radius pointsArray:points andNormalsArray:norms];
    [self addPoint:v23 withIndex:i23 radius:radius pointsArray:points andNormalsArray:norms];
    [self addPoint:v31 withIndex:i31 radius:radius pointsArray:points andNormalsArray:norms];
    
    [self subdivideTriangleWithIndices:i1 index2:i12 index3:i31 depth:depth-1 radius:radius length:len pointsArray:points normalsArray:norms andTriangles:tris];
    [self subdivideTriangleWithIndices:i2 index2:i23 index3:i12 depth:depth-1 radius:radius length:len pointsArray:points normalsArray:norms andTriangles:tris];
    [self subdivideTriangleWithIndices:i3 index2:i31 index3:i23 depth:depth-1 radius:radius length:len pointsArray:points normalsArray:norms andTriangles:tris];
    [self subdivideTriangleWithIndices:i12 index2:i23 index3:i31 depth:depth-1 radius:radius length:len pointsArray:points normalsArray:norms andTriangles:tris];
}

/**
 *  The intermediate function called recursively by the makeSphere:withDepth:andRadius: method.
 *
 *  @param pt A point on the sphere.
 *  @param i Tthe index of the point.
 *  @param radius The radius of the sphere.
 *  @param points A pointer to the points of the sphere.
 *  @param norms A pointer to the normals of the sphere.
 */

- (void)addPoint:(Vectormath::Aos::Point3)pt
       withIndex:(unsigned int)i
          radius:(REAL)radius
     pointsArray:(col::Pnt3Array *)points
 andNormalsArray:(col::Vec3Array *)norms
{
    Vectormath::Aos::Vector3 v3( pt[0], pt[1], pt[2] );
    Vectormath::Aos::Vector3 norm = normalize( v3 );
    norms->push_back( norm );
    
    Vectormath::Aos::Point3 p( norm * radius );
    points->push_back( p );
}

/**
 *  A convenience method which generates the normals of a given geometry by going through all the faces and calculating the cross product of
 *  two adjacent vectors.
 *
 *  @param geo A pointer to the geometry of the mesh.
 */

- (void)addNormalsToGeometry:(col::ColGeometry *)geo
{
    // Get an immutable reference to the points and primitives of the geometry
    col::Pnt3Array const points = geo->getPoints();
    col::PrimArray const primitives = geo->getPrimitives();
    
    for (unsigned int i = 0; i < primitives.size(); i++)
        for (unsigned int j = 0; j < primitives[i].size(); j++) {
            // Generate the cross product between the two adjacent vectors, normalize it and save it for every vertex in the given
            // triangle in our normals mutable pointer
            col::Point3f a = points[primitives[i][0]];
            col::Point3f b = points[primitives[i][1]];
            col::Point3f c = points[primitives[i][2]];
            
            col::Vector3f v1 = b - a;
            col::Vector3f v2 = c - a;
            Vector3 v = col::cross(v1, v2);
            v = col::normalize(v);
            
            geo->editNormalsPtr()->push_back(v);
        }
}

#pragma mark -
#pragma mark Texture Creation
#pragma mark -

/** @name Texture creation */

/**
 *  A convenience method used to create a texture from a file with a given filename and a color format, i.e. *GL_RGB*, *GL_RGBA*, etc.
 *
 *  @param filename The filename of the file.
 *  @param options The options which we will use when loading the texture into the memory.
 *
 *  @return Returns a *GLKTextureInfo* instance which contains all informations about the texture.
 */

- (GLKTextureInfo *)textureFromImageWithFilename:(NSString *)filename
                                          ofType:(ColTextureType)textureType
                                         options:(NSDictionary *)options
{
    NSString *filepath;
    NSError *error = nil;
    
    switch (textureType) {
        case ColTextureTypePVR:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"pvr"];
            break;
        case ColTextureTypePNG:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"png"];
            break;
        case ColTextureTypeJPEG:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"jpg"];
            break;
    }
    GLKTextureInfo *textureInfo = [GLKTextureLoader textureWithContentsOfFile:filepath options:options error:&error];
    
    return (!error) ? textureInfo : nil;
}

/**
 *  A convenience method used to create a texture *asynchronously* (see textureFromImageWithFilename:options: for the synchronous equivalent of this method) from a file with a given filename and a color format, i.e. *GL_RGB*, *GL_RGBA*, etc.
 *
 *  @param filename The filename of the file.
 *  @param options The options which we will use when loading the texture into the memory.
 *  @param completionHandler The block which returns the *GLKTextureInfo* object after the texture has been loaded.
 */

- (void)textureAsynchronouslyFromImageWithFilename:(NSString *)filename
                                            ofType:(ColTextureType)textureType
                                           options:(NSDictionary *)options
                                        completion:(void (^)(GLKTextureInfo *textureInfo, NSError *outError))completionHandler
{
    NSString *filepath;
    
    switch (textureType) {
        case ColTextureTypePVR:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"pvr"];
            break;
        case ColTextureTypePNG:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"png"];
            break;
        case ColTextureTypeJPEG:
            filepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"jpg"];
            break;
    }
    
    if (!_textureLoader)
        _textureLoader = [[GLKTextureLoader alloc] init];
    
    [_textureLoader textureWithContentsOfFile:filepath
                                      options:options
                                        queue:NULL
                            completionHandler:^(GLKTextureInfo *textureInfo, NSError *outError) {
                                if (completionHandler)
                                    completionHandler(textureInfo, outError);
                            }];
}

#pragma mark -
#pragma mark Vertex Array Objects
#pragma mark -

/** @name Memory allocation and deallocation */

/**
 *  Convenience method which creates and binds buffers for a given geometry. This is a method you MUST call before you draw the object.
 *
 *  @param vao A pointer to an already created vertex array object.
 *  @param vbo A pointer to an already created vertex buffer object.
 *  @param ibo A pointer to an already created buffer which stores the indices for the geometry.
 *  @param geo A pointer to the geometry of the mesh.
 *  @param vertexAttrib A vertex attribute id of type unsigned int which will be used in the shaders to bind the vertex data.
 *  @param normalAttrib A normal attribute id of type unsigned int which will be used in the shaders to bind the data for the normals.
 */

- (void)createVertexArray:(GLuint& )vao
    forVertexBufferObject:(GLuint& )vbo
              forGeometry:(col::ColGeometry const *)geo
             geometryType:(ColGeometryType)geometryType
{
    // Generate the vertex array object
    glGenVertexArraysOES(1, &vao);
    glBindVertexArrayOES(vao);
    
    if (geometryType != ColGeometryTypeOther) {
        // Generate the vertex buffer object
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        
        BOOL hasTexture = NO;
        switch(geometryType) {
            case ColGeometryTypeCrate:
                glBufferData(GL_ARRAY_BUFFER, sizeof(CrateVerts), CrateVerts, GL_STATIC_DRAW);
                hasTexture = CrateHasTexture;
                break;
            case ColGeometryTypeCage:
                glBufferData(GL_ARRAY_BUFFER, sizeof(CageVerts), CageVerts, GL_STATIC_DRAW);
                hasTexture = CageHasTexture;
                break;
            case ColGeometryTypeBarChair:
                glBufferData(GL_ARRAY_BUFFER, sizeof(BarChairVerts), BarChairVerts, GL_STATIC_DRAW);
                hasTexture = BarChairHasTexture;
                break;
            case ColGeometryTypeBall:
                glBufferData(GL_ARRAY_BUFFER, sizeof(GolfBallVerts), GolfBallVerts, GL_STATIC_DRAW);
                hasTexture = GolfBallHasTexture;
                break;
            case ColGeometryTypeTable:
                glBufferData(GL_ARRAY_BUFFER, sizeof(TableVerts), TableVerts, GL_STATIC_DRAW);
                hasTexture = TableHasTexture;
                break;
            case ColGeometryTypeVase:
                glBufferData(GL_ARRAY_BUFFER, sizeof(VaseVerts), VaseVerts, GL_STATIC_DRAW);
                hasTexture = VaseHasTexture;
                break;
            case ColGeometryTypeMushroom:
                glBufferData(GL_ARRAY_BUFFER, sizeof(MushroomVerts), MushroomVerts, GL_STATIC_DRAW);
                hasTexture = MushroomHasTexture;
                break;
            case ColGeometryTypeOther:
                break;
            default:
                break;
        }
        
        GLsizei stride = sizeof(float) * 6;
        if (hasTexture)
            stride += sizeof(float) * 2;
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, stride, 0);
        glEnableVertexAttribArray(GLKVertexAttribNormal);
        glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizeof(float) * 3));
        if (hasTexture) {
            glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
            glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizeof(float) * 6));
        }
    }
    else {
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glEnableVertexAttribArray(GLKVertexAttribNormal);
        
        col::PrimArray faces = geo->getPrimitives();
        
        // Generate the vertex buffer object
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * faces[0].size() * faces.size(), &faces[0], GL_STATIC_DRAW);
    }
    glBindVertexArrayOES(0);
}

/**
 *  Convenience method which destroys the 3 buffers created by the method createVertexArray:forVertexBufferObject:andIndexBuffer:forGeometry:withVerticesDataBuffer:indicesBuffer:vertexAttribute:andNormalAttribute:
 *
 *  @param vao A pointer to an already created vertex array object.
 *  @param vbo A pointer to an already created vertex buffer object.
 */

- (void)deleteVertexArray:(GLuint& )vao
    forVertexBufferObject:(GLuint& )vbo
{
    glDeleteBuffers(1, &vao);
    glDeleteVertexArraysOES(1, &vao);
}

#pragma mark -
#pragma mark Random Number Generator
#pragma mark -

/** @name Miscellaneous */

/**
 *  Generate a random float number between zero and one
 *
 *  @return Returns a random float number between _0_ and _1_.
 */

- (float)randomFloatBetweenZeroAndOne
{
    return ((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX);
}

#pragma mark -
#pragma mark Shaders Initialization
#pragma mark -

/** Shaders initialization */

/**
 *  A convenience method which compiles a given shader pair for a given program with specified uniforms and attributes
 *
 *  @param      filename        The name of the shaders stored on the device of type _.vsh_ (vertex shader) and _.fsh_ (fragment shader)
 *  @param      program         A pointer to an OpenGL program object of type _GLuint_.
 *  @param      attributes      An object of type _NSDictionary_ containing all attributes to be bound to the program, the general format is
 *
 *
 *
 *                              `@(ATTRIB_NAME1), @"attrib_name_in_shader1"`
 *                              `@(ATTRIB_NAME2), @"attrib_name_in_shader2"`
 *                              etc.
 *
 *
 *
 *                              We use these later in our _glVertexAttribPointer()_ function.
 *  @param      unifsDict       An object of type _NSDictionary_ containing all uniforms to be bound to the program, the general format is
 *
 *
 *
 *                              `@(UNIFORM_NAME1), @"uniform_name_in_shader1"`
 *                              `@(UNIFORM_NAME2), @"uniform_name_in_shader2"`
 *                              etc.
 *
 *
 *
 *                              We use these later before every draw to change the uniform variables.
 *  @param      unifs           A pointer to an array of type _GLuint_, containing all uniforms which locations are going to be used
 *                              after the shader program compilation.
 *
 *  @return     The method returns _NO_ if it's successful, _YES_ otherwise
 */

- (BOOL)loadShadersWithFilename:(NSString *)filename
                     forProgram:(GLuint *)program
                 withAttributes:(NSDictionary *)attributes
                    andUniforms:(NSDictionary *)unifsDict
              withUniformsArray:(GLint *)unifs
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    *program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:filename ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return YES;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:filename ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return YES;
    }
    
    // Attach vertex shader to program.
    glAttachShader(*program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(*program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking
    [attributes enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        glBindAttribLocation(*program, [(NSNumber *)object intValue], [(NSString *)key UTF8String]);
    }];
    
    // Link program.
    if (![self linkProgram:*program]) {
        NSLog(@"Failed to link program: %d", *program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (*program) {
            glDeleteProgram(*program);
            *program = 0;
        }
        
        return YES;
    }
    
    // Get uniform locations using a block to enumerate through the keys and values
    [unifsDict enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        int location = [(NSNumber *)object intValue];
        unifs[location] = glGetUniformLocation(*program, [(NSString *)key UTF8String]);
    }];
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(*program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(*program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return NO;
}

/**
 *  A method to compile a shader (vertex or fragment) with a given filename, it gets used in the more general method
 *  loadShadersWithFilename:forProgram:withAttributes:andUniforms:withUniformsArray:
 *
 *  @param shader A pointer to a shader (vertex or fragment).
 *  @param type The type of the shader (vertex or fragment).
 *  @param file The filename of the shader.
 *
 *  @return It returns _YES_ if successful, _NO_ otherwise
 */

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load the shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

/**
 *  A method used to link a given program, it gets called in the more general method loadShadersWithFilename:forProgram:withAttributes:andUniforms:withUniformsArray:
 *
 *  @param prog A shader program.
 *
 *  @return Returns _YES_ if successful, _NO_ otherwise.
 */

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

/**
 *  A method used to validate a certain shader program. It can be used for debugging purposes.
 *
 *  @param prog A shader program.
 *
 *  @return Returns _YES_ if successful, _NO_ otherwise.
 */

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end
