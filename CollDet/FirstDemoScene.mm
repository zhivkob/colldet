//
//  FirstDemoScene.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import <CoreMotion/CoreMotion.h>
#import "FirstDemoScene.h"
#import "Constants.h"
#import "ColUtilsGLES.h"

static unsigned int framesCount = 0;

@interface FirstDemoScene()
{
    // Objects that we will need for the collision detection
    col::CollisionPipeline *_pipeline;
    
    // Base effect (mimics shaders)
    GLKBaseEffect *_baseEffect;
    
    // Variables we will need to present our moving objects to the screen
    NSMutableArray *_balls;
    
    // Variables we will need to present the cage to the screen
    CollidableObject *_collidableCage;
    
    // Global variable for the GL utility methods
    ColUtilsGLES *_colUtils;
    
    // Projection and normal matrices
    GLKMatrix4 _projectionMatrix;
    GLKMatrix4 _modelviewMatrix;
    GLKMatrix4 lookAtTransformation;
    CGSize _screenSize;
    
    // The audio controller for the scene
    AudioController *_audioController;
}

@end

@implementation FirstDemoScene

- (id)init
{
    if (self = [super init]) {
        col::DopTree::init();
        
        // Initialize the collision detection pipeline with a given name and id
        _pipeline = new col::CollisionPipeline ("RollingBallCollisionThread", 1);
        _pipeline->M_PipelineAlgorithm = col::ALGO_BOXTREE;
        _pipeline->setUseHulls(false);
        _pipeline->verbose(false, false);
        
        // Initialize the base effect
        _baseEffect = [[GLKBaseEffect alloc] init];
        
        // Initialize the objects in the scene
        _balls = [NSMutableArray arrayWithCapacity:BallsCount];
        
        // Get a reference to the collision utilities class
        _colUtils = [ColUtilsGLES sharedUtils];
        
        _screenSize = [[UIScreen mainScreen] bounds].size;
        
        // Set the matrices
        _baseTransformation = GLKMatrix4Identity;
        _modelviewMatrix = GLKMatrix4Identity;
        _projectionMatrix = GLKMatrix4Identity;
        
        // Create the audio player
        _audioController = [AudioController sharedController];
        [_audioController createBufferForFileWithFilename:@"vase" ofType:@"caf" looping:NO];
        
        [self setupGL];
        [self setupCollidableObjects];
    }
    return self;
}

- (void)dealloc
{
    delete _pipeline;
}

- (void)setupGL
{
    _baseEffect.texture2d0.enabled = GL_TRUE;
    _baseEffect.texture2d0.envMode = GLKTextureEnvModeModulate;
    _baseEffect.light0.enabled = GL_TRUE;
    _baseEffect.light1.enabled = GL_TRUE;
    _baseEffect.colorMaterialEnabled = GL_TRUE;
    
    lookAtTransformation = GLKMatrix4Multiply(TranslationFirstDemo, LookAtFirstDemo);
    float aspect = fabsf(_screenSize.height / _screenSize.width);
    _projectionMatrix = GLKMatrix4MakePerspective(FovyRadians, aspect, ZNear, ZFar);
    _baseEffect.transform.projectionMatrix = _projectionMatrix;
}

- (void)setupCollidableObjects
{
    _collidableCage = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_collidableCage setCustomModelGeometryOfType:ColGeometryTypeCage cachedFromObject:nil];
    [_collidableCage setTextureWithFilename:@"ConcreteFloor" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:nil];
    GLKMatrix4 scale = GLKMatrix4MakeScale(2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f));
    GLKMatrix4 translate = GLKMatrix4Translate(_collidableCage.currentMatrix, 0.0f, 0.0f, CageRadius + 0.5f);
    _collidableCage.currentMatrix = GLKMatrix4Multiply(translate, scale);
    
    for (int i = 0; i < BallsCount; i++) {
        CollidableObject *ball = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
        // Create the geometry for every moving object (use an already created one as a cache too)
        if (i == 0) {
            [ball setCustomModelGeometryOfType:ColGeometryTypeVase cachedFromObject:nil];
            [ball setTextureWithFilename:@"Vase" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:nil];
        }
        else {
            [ball setCustomModelGeometryOfType:ColGeometryTypeVase cachedFromObject:[_balls objectAtIndex:0]];
            [ball setTextureWithFilename:@"Vase" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:[_balls objectAtIndex:0]];
        }

        // The transformation to set the spheres in a gain
        ball.currentMatrix = GLKMatrix4MakeTranslation(SpaceBetweenBalls * (i - BallsCount / 2.0f + 1.0f), 0.0f, 0.0f);
        [ball setRandomColor];

        // Register the collidable objects
        [ball makeCollidable];
        
        [_balls addObject:ball];
    }
    
    // Register callbacks for each pair of objects
    // Here you can specify which callback class should be used
    for (unsigned int i = 0; i < BallsCount; i++)
        for (unsigned int j = 0; j < i; j++) {
            CollisionCallback *collisionCallback = new CollisionCallback([_balls objectAtIndex:j], [_balls objectAtIndex:i], LevelOfDetection);
            collisionCallback->collisionBlock = ^(CollidableObject *collObject1, CollidableObject *collObject2) {
                // If one of both objects is already colliding, then skip the procedure
                //
                if (!collObject1.isColliding && !collObject2.isColliding) {
                    [_audioController playSound:@"vase" looping:NO];
                    
                    // Calculate the collision normal for both objects
                    GLKVector3 object1Position = GLKVector3Make(collObject1.currentMatrix.m30, collObject1.currentMatrix.m31, collObject1.currentMatrix.m32);
                    GLKVector3 object2Position = GLKVector3Make(collObject2.currentMatrix.m30, collObject2.currentMatrix.m31, collObject2.currentMatrix.m32);
                    GLKVector3 normal = GLKVector3Normalize(GLKVector3Subtract(object1Position, object2Position));
                    [collObject1 setCollisionNormal:GLKVector3Negate(normal) forHitFrame:framesCount];
                    [collObject1 invertVelocity];
                    [collObject2 setCollisionNormal:normal forHitFrame:framesCount];
                    [collObject2 invertVelocity];
                }
            };
            _pipeline->addCallback(collisionCallback);
        }
}

- (void)update
{
    for (CollidableObject *collObject in _balls) {
        if(collObject.currentMatrix.m30 > RightBound || collObject.currentMatrix.m30 < LeftBound) {
            collObject.translVelocity = GLKVector3Negate(collObject.translVelocity);
            collObject.rotAngle = -collObject.rotAngle;
            [_audioController playSound:@"vase" looping:NO];
        }

        GLKMatrix4 translation = GLKMatrix4TranslateFromMatrix(collObject.currentMatrix);
        translation = GLKMatrix4TranslateWithVector3(translation, collObject.translVelocity);
        GLKMatrix4 rotation = GLKMatrix4Identity;
        if (BallsRotate) {
            rotation = GLKMatrix4RotationFromMatrix(collObject.currentMatrix);
            rotation = GLKMatrix4Rotate(rotation, collObject.rotAngle, collObject.rotAxis.x, collObject.rotAxis.y, collObject.rotAxis.z);
        }
        
        collObject.currentMatrix = GLKMatrix4Multiply(translation, rotation);
        [collObject updatePipeline];
    }
        
    // Check for collisions on the pipeline
    _pipeline->check();
    framesCount++;
}

- (void)draw
{
    // Draw the moving balls on the screen
    for (CollidableObject *ball in _balls) {
        _modelviewMatrix = GLKMatrix4Multiply(_baseTransformation, ball.currentMatrix);
        _modelviewMatrix = GLKMatrix4Multiply(lookAtTransformation, _modelviewMatrix);
        _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
        _baseEffect.texture2d0.name = ball.textureInfo.name;
        [ball drawWithMode:GL_TRIANGLES];
    }

    // Draw the ground plane on the screen
    _modelviewMatrix = GLKMatrix4Multiply(_baseTransformation, _collidableCage.currentMatrix);
    _modelviewMatrix = GLKMatrix4Multiply(lookAtTransformation, _modelviewMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    _baseEffect.texture2d0.name = _collidableCage.textureInfo.name;
    [_collidableCage drawWithMode:GL_TRIANGLES];
}
    
@end

