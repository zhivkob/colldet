//
//  Icosahedron.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <vectormath/scalar/cpp/vectormath_aos.h>

using namespace Vectormath::Aos;

const unsigned int IcosahedronPointsCount = 12;
const unsigned int IcosahedronIndicesCount = 20;

const REAL IcosahedronX = 0.525731112119133606;
const REAL IcosahedronZ = 0.850650808352039932;

/**
 *  12 Vertices of standard icosahedron
 */
const Point3 IcosahedronPoints[IcosahedronPointsCount] = {
    Point3(	 -IcosahedronX, 0.0,   IcosahedronZ ),
    Point3(   IcosahedronX, 0.0,   IcosahedronZ ),
    Point3(  -IcosahedronX, 0.0,  -IcosahedronZ ),
    Point3(   IcosahedronX, 0.0,  -IcosahedronZ ),
    Point3( 0.0,   IcosahedronZ,   IcosahedronX ),
    Point3( 0.0,   IcosahedronZ,  -IcosahedronX ),
    Point3( 0.0,  -IcosahedronZ,   IcosahedronX ),
    Point3( 0.0,  -IcosahedronZ,  -IcosahedronX ),
    Point3(   IcosahedronZ,   IcosahedronX, 0.0 ),
    Point3(	 -IcosahedronZ,   IcosahedronX, 0.0 ),
    Point3(   IcosahedronZ,  -IcosahedronX, 0.0 ),
    Point3(  -IcosahedronZ,  -IcosahedronX, 0.0 )
};

/**
 *  20 Triangles of standard icosahedron
 */
const unsigned int IcosahedronIndices[IcosahedronIndicesCount * 3] = {
    1, 4, 0,
    4, 9, 0,
    4, 5, 9,
    8, 5, 4,
    1, 8, 4,
    1,10, 8,
    10, 3, 8,
    8, 3, 5,
    3, 2, 5,
    3, 7, 2,
    3,10, 7,
    10, 6, 7,
    6,11, 7,
    6, 0,11,
    6, 1, 0,
    10, 1, 6,
    11, 0, 9,
    2,11, 9,
    5, 2, 9,
    11, 2, 7
};
