//
//  MainViewController.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController()
{
    MenuScene *_menuScene;
    FirstDemoScene *_firstDemoScene;
    SecondDemoScene *_secondDemoScene;
    
    // Save the scene we are currently displaying in a state variable
    Scene _currentScene;
    
    AudioController *_audioController;
    ColUtilsGLES *_colUtils;
    
    // Variables used in the rotation of the scene with fingers
    GLKQuaternion _quaternion;
    CGPoint _initLocation;
    
    // Variable used for the automatic rotation of the first scene
    CGFloat _rotation;
    
    // Variables we use for scaling the scene
    CGFloat _scale;
    CGFloat _lastScale;
}

@property (strong, nonatomic) EAGLContext *context;
@property (weak, nonatomic) IBOutlet UIButton *firstDemoButton;
@property (weak, nonatomic) IBOutlet UIButton *secondDemoButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;

// Setup openAL
- (void)setupAL;

// Setup OpenGL ES
- (void)setupGL;
- (void)tearDownGL;

// Setup the user interface
- (void)setupUserInterface;

// User actions
- (IBAction)pauseButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;
- (IBAction)firstDemoButtonPressed:(id)sender;
- (IBAction)secondDemoButtonPressed:(id)sender;
- (IBAction)doubleTapGestureRecognized:(UITapGestureRecognizer *)sender;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Setup the OpenGL context and initialize the scenes here
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableColorFormat = GLKViewDrawableColorFormatRGBA8888;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    view.drawableMultisample = GLKViewDrawableMultisample4X;
    [EAGLContext setCurrentContext:self.context];
    
    [self setupUserInterface];
    [self setupAL];
    _currentScene = SCENE_MENU;
    
    // Setup the variables to use in the rotation/scale of the scene
    _scale = 2.0f;
    _lastScale = 1.0f;
    _quaternion = GLKQuaternionIdentity;
    _initLocation = CGPointZero;
    
    [self setupGL];
    
    if (_menuScene == nil)
        _menuScene = [[MenuScene alloc] init];
    
    if (_firstDemoScene == nil)
        _firstDemoScene = [[FirstDemoScene alloc] init];
    
    if (_secondDemoScene == nil)
        _secondDemoScene = [[SecondDemoScene alloc] init];
    
    [_menuScene startMotionUpdates];
}

- (void)dealloc
{
    [self tearDownGL];
    [_audioController cleanOpenAL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
	self.context = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

// Setup openAL
- (void)setupAL
{
    // Get the shared instance of the audio controller and create a buffer for the audio file to seemlessly play it afterwards
    _audioController = [AudioController sharedController];
    [_audioController createBufferForFileWithFilename:@"click" ofType:@"caf" looping:NO];
}

- (void)setupGL
{
    glEnable(GL_DEPTH_TEST);
}

// Tear down all OpenGL scenes here
- (void)tearDownGL
{
    [_menuScene tearDownGL];
    [_firstDemoScene tearDownGL];
    [_secondDemoScene tearDownGL];
    
    [EAGLContext setCurrentContext:self.context];
}

// Setup the user interface here
- (void)setupUserInterface
{
    UIImage *resizableImage;
    if (!IS_IPAD)
        resizableImage = [[UIImage imageNamed:@"btn-demo"] resizableImageWithCapInsets:UIEdgeInsetsMake(15.0f, 15.0f, 15.0f, 15.0f)];
    else
        resizableImage = [[UIImage imageNamed:@"btn-demo-ipad"] resizableImageWithCapInsets:UIEdgeInsetsMake(35.0f, 35.0f, 35.0f, 35.0f)];
    
    [_firstDemoButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
    [_secondDemoButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
    [_backButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
    [_pauseButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
}

- (void)rotateQuaternionWithVector:(CGPoint)delta
{
	GLKVector3 up = GLKVector3Make(0.0f, 1.0f, 0.0f);
	GLKVector3 right = GLKVector3Make(-1.0f, 0.0f, 0.0f);
    
	up = GLKQuaternionRotateVector3(GLKQuaternionInvert(_quaternion), up);
	_quaternion = GLKQuaternionMultiply(_quaternion, GLKQuaternionMakeWithAngleAndVector3Axis(delta.x * RADIANS_PER_PIXEL, up));
    
	right = GLKQuaternionRotateVector3(GLKQuaternionInvert(_quaternion), right);
	_quaternion = GLKQuaternionMultiply(_quaternion, GLKQuaternionMakeWithAngleAndVector3Axis(delta.y * RADIANS_PER_PIXEL, right));
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self.view];
    
	_initLocation = location;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self.view];
    
	// get touch delta
	CGPoint delta = CGPointMake(location.x - _initLocation.x, -(location.y - _initLocation.y));
	_initLocation = location;
    
	// rotate
	[self rotateQuaternionWithVector:delta];
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    switch (_currentScene) {
        case SCENE_MENU: {
            [_menuScene update];
            
            break;
        }
        case SCENE_FIRST_DEMO: {
            // Update the rotation angle and set the transformation matrix for the first demo scene
            _rotation += SceneRotationAngle;
            GLKMatrix4 baseTransformation = GLKMatrix4MakeRotation(GLKMathDegreesToRadians(_rotation), 0.0f, 0.0f, 1.0f);
            _firstDemoScene.baseTransformation = GLKMatrix4Scale(baseTransformation, _scale * 1.4f, _scale * 1.4f, _scale * 1.4f);
            [_firstDemoScene update];
            
            break;
        }
        case SCENE_SECOND_DEMO: {
            // Update the transformation matrix based on rotation and scale by the user
            GLKVector3 axis = GLKQuaternionAxis(_quaternion);
            float angle = GLKQuaternionAngle(_quaternion);
            if (angle)
                _secondDemoScene.baseTransformation = GLKMatrix4MakeRotation(angle, axis.x, axis.y, axis.z);
            
            [_secondDemoScene update];
            
            break;
        }
    }
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    
    switch (_currentScene) {
        case SCENE_MENU:
            [_menuScene draw];
            break;
        case SCENE_FIRST_DEMO:
            [_firstDemoScene draw];
            break;
        case SCENE_SECOND_DEMO:
            [_secondDemoScene draw];
            break;
        default:
            break;
    }

    glDiscardFramebufferEXT(GL_FRAMEBUFFER, 2, discards);
}

- (IBAction)pauseButtonPressed:(id)sender
{
    [_audioController playSound:@"click" looping:NO];
    self.paused = !self.paused;
    if (self.paused)
        [self.pauseButton setTitle:@"Play" forState:UIControlStateNormal];
    else
        [self.pauseButton setTitle:@"Pause" forState:UIControlStateNormal];
}

- (IBAction)backButtonPressed:(id)sender
{
    [_audioController playSound:@"click" looping:NO];
    [self.pauseButton setHidden:YES];
    [self.backButton setHidden:YES];
    [self.firstDemoButton setHidden:NO];
    [self.secondDemoButton setHidden:NO];
    
    if (self.paused)
        self.paused = !self.paused;
    
    _currentScene = SCENE_MENU;
    [_menuScene startMotionUpdates];
}

- (IBAction)firstDemoButtonPressed:(id)sender
{
    [_audioController playSound:@"click" looping:NO];
    [self.pauseButton setHidden:NO];
    [self.backButton setHidden:NO];
    [self.firstDemoButton setHidden:YES];
    [self.secondDemoButton setHidden:YES];

    _currentScene = SCENE_FIRST_DEMO;
    [_menuScene stopMotionUpdates];
}

- (IBAction)secondDemoButtonPressed:(id)sender
{
    [_audioController playSound:@"click" looping:NO];
    [self.pauseButton setHidden:NO];
    [self.backButton setHidden:NO];
    [self.firstDemoButton setHidden:YES];
    [self.secondDemoButton setHidden:YES];
    
    _currentScene = SCENE_SECOND_DEMO;
    [_menuScene stopMotionUpdates];
}

- (IBAction)doubleTapGestureRecognized:(UIPanGestureRecognizer *)sender
{
    _scale = 1.0f;
}

- (IBAction)zoomView:(UIPinchGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateEnded)
		_lastScale = 1.0f;
	else
	{
		_scale = _scale - (_lastScale - sender.scale);
		_lastScale = sender.scale;
	}
}

@end
