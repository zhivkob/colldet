
//---------------------------------------------------------------------------
//                              ColIntersect
//---------------------------------------------------------------------------
//  Copyright (C):
//---------------------------------------------------------------------------
//CVSId: "@(#)$Id: ColIntersect.h,v 1.4 2004/02/26 14:50:17 ehlgen Exp $"


#ifndef ColIntersect_H
#define ColIntersect_H
#if defined(__sgi) || defined(_WIN32)
#pragma once
#endif


//---------------------------------------------------------------------------
//  Includes
//---------------------------------------------------------------------------

#include <math.h>

#include <col_import_export.h>
#include <ColGeometry.h>

namespace col {

extern int vvv;
//---------------------------------------------------------------------------
//  Constants
//---------------------------------------------------------------------------

/** Maximal number of vertices a polygon is allowed to contain. 
 *  On polygons having up to MaxNVertices the intersectPolygon routines are
 *  able to perform a coordinate transformation.
 *  This maximum was introduced for performance reasons.
 */
const unsigned int MaxNVertices = 10;

//---------------------------------------------------------------------------
//  Functions
//---------------------------------------------------------------------------

// Entry for intersection test
COL_EXPORTIMPORT
bool intersectPolygons(const Point3f *poly1, int plSize1,
					   const Point3f *poly2, int plSize2,
					   const unsigned int *index1 = NULL,
					   const unsigned int *index2 = NULL,
					   const Matrix4 *cxform = NULL );

// Low level tests
COL_EXPORTIMPORT
bool intersectCoplanarTriangles( const Vector3f &normalV,
								 const Point3f &polyVv0,
								 const Point3f &polyVv1,
								 const Point3f &polyVv2,
								 const Point3f &polyUv0,
								 const Point3f &polyUv1,
								 const Point3f &polyUv2 );

COL_EXPORTIMPORT
bool intersectQuadrangles( const Point3f &polyVv0,
						   const Point3f &polyVv1,
						   const Point3f &polyVv2,
						   const Point3f &polyVv3,
						   const Point3f &polyUv0,
						   const Point3f &polyUv1,
						   const Point3f &polyUv2,
						   const Point3f &polyUv3,
						   const Vector3f &normal1V,
						   const Vector3f &normal2V );

COL_EXPORTIMPORT
bool intersectTriangles( const Point3f &polyVv0,
						 const Point3f &polyVv1,
						 const Point3f &polyVv2,
						 const Point3f &polyUv0,
						 const Point3f &polyUv1,
						 const Point3f &polyUv2 );

COL_EXPORTIMPORT
bool intersectTriangles( const Point3f &polyVv0,
						 const Point3f &polyVv1,
						 const Point3f &polyVv2,
						 const Point3f &polyUv0,
						 const Point3f &polyUv1,
						 const Point3f &polyUv2,
						 const Vector3f &n1,
						 const Vector3f &n2 );

COL_EXPORTIMPORT
bool intersectEdgePolygon( const Point3f &v1, const Point3f &v2,
						   const Point3f *poly, int c,
						   const Vector3f &normalV,
						   unsigned int x, unsigned int y );

COL_EXPORTIMPORT
bool intersectEdgePolygon( const Point3f &v1, const Point3f &v2,
						   const Point3f *poly, unsigned int plSize );

COL_EXPORTIMPORT
bool intersectArbPolygons( const Point3f *poly1, unsigned int plSize1,
						   const Point3f *poly2, unsigned int plSize2);

COL_EXPORTIMPORT
bool intersectArbPolygons( const Point3f *poly1, unsigned int plSize1,
						   const Point3f *poly2, unsigned int plSize2,
						   const Vector3f &normal1V,
						   const Vector3f &normal2V );
								  
COL_EXPORTIMPORT
bool intersectCoplanarEdges( const Point3f &v0V, const Point3f &v1V,
							 const Point3f &u0V, const Point3f &u1V,
							 unsigned int x, unsigned int y);

COL_EXPORTIMPORT
bool computeIntervals( REAL vv0, REAL vv1, REAL vv2,
					   REAL d0,  REAL d1,  REAL d2,
					   REAL d0d1, REAL d0d2,
					   REAL &isect0, REAL &isect1, REAL epsilon);

COL_EXPORTIMPORT
void isect(REAL vv0, REAL vv1, REAL vv2, REAL d0, REAL d1, REAL d2,
		   REAL *isect0, REAL *isect1);

}// namespace col

#endif //ColIntersect_H


