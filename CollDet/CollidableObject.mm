//
//  CollidableObject.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import "CollidableObject.h"

@interface CollidableObject()

/**
 Returns the _colId_ of the object returned when making the object collidable.
 */
@property (nonatomic) col::ColID colId;

/**
 Returns a reference to the geometry of the object.
 */
@property (nonatomic) col::ColGeometry geometry;

/**
 Returns the type of the geometry.
 */
@property (nonatomic) ColGeometryType geometryType;

/**
 Returns a _GLKTextureInfo_ object containing the texture information.
 */
@property (strong, nonatomic) GLKTextureInfo *textureInfo;

/**
 Returns the vertex buffer object used for drawing.
 */
@property (nonatomic) GLuint vertexBufferObject;

/**
 Returns the vertex array object used for drawing.
 */
@property (nonatomic) GLuint vertexArrayObject;

/**
 Returns the index buffer object used for drawing.
 */
@property (nonatomic) GLuint indexBufferObject;

@end

@implementation CollidableObject

/** @name Initialization */

/**
 *  Initializer using the pipeline as an argument.
 *
 *  @param pipl Reference to the pipeline to be used with the object.
 *  @param baseEffect Reference to the base effect which we use to prepare the scene for rendering. (mimics a simple shader)
 *
 */
- (id)initWithPipeline:(col::CollisionPipeline *)pipl baseEffect:(GLKBaseEffect  *)baseEffect
{
    if (self = [super init]) {
        _pipeline = pipl;
        _baseEffect = baseEffect;
        _colUtils = [ColUtilsGLES sharedUtils];
        
        _currentMatrix = GLKMatrix4Identity;
        _isColliding = NO;
        _hitFrame = 0;
        _translVelocity = GLKVector3Make(MovementOffset, 0.0f, 0.0f);
        _rotAxis = RotationAxis;
        _rotAngle = RotationAngle;
        _rotVelocity = GLKMatrix4Identity;
        _oldMatrix = GLKMatrix4Identity;
        _collisionNormal = GLKVector3Make(-1.0f, 0.0f, 0.0f);
        _diffuseColor = GLKVector4Make(0.3f, 0.4f, 0.7f, 1.0f);
        _ambientColor = GLKVector4Make(0.4f, 0.4f, 0.4f, 1.0f);
        _specularColor = GLKVector4Make(0.6f, 0.6f, 0.6f, 1.0f);
        _shininess = 64.0f;
    }
    return self;
}

/** @name Updating the properties */

/**
 *
 * Makes the object collidable
 *
 */
- (void)makeCollidable
{
    _colId = _pipeline->makeCollidable(static_cast<void *>(&_geometry), NULL);
}

/**
 *
 * Updates the pipeline using the current object matrix.
 *
 */
- (void)updatePipeline
{
    Matrix4 translation = ColMatrix4MakeWithGLKMatrix4(_currentMatrix);
    _pipeline->moveObject(_colId, static_cast<void*>(&translation), NULL);
}

/**
 *
 * Sets a random color to the object
 *
 */
- (void)setRandomColor
{
    _diffuseColor = GLKVector4Make([_colUtils randomFloatBetweenZeroAndOne],
                                   [_colUtils randomFloatBetweenZeroAndOne],
                                   [_colUtils randomFloatBetweenZeroAndOne],
                                   1.0f);
    _ambientColor = GLKVector4Make([_colUtils randomFloatBetweenZeroAndOne] / 3.0f,
                                   [_colUtils randomFloatBetweenZeroAndOne] / 3.0f,
                                   [_colUtils randomFloatBetweenZeroAndOne] / 3.0f,
                                   1.0f);
    _specularColor = GLKVector4Make([_colUtils randomFloatBetweenZeroAndOne] / 2.0f,
                                    [_colUtils randomFloatBetweenZeroAndOne] / 2.0f,
                                    [_colUtils randomFloatBetweenZeroAndOne] / 2.0f,
                                    1.0f);
    _shininess = [_colUtils randomFloatBetweenZeroAndOne] * 64.0f;
}

/**
 *
 *  Sets a random position of the object and updates the matrix.
 *
 *  @param width The width of the box.
 *
 */
- (void)setRandomPositionInBoxWithWidth:(CGFloat)width
{
    _currentMatrix = GLKMatrix4Translate(_currentMatrix,
                                         ([_colUtils randomFloatBetweenZeroAndOne] - 0.5f) * width,
                                         ([_colUtils randomFloatBetweenZeroAndOne] - 0.5f) * width,
                                         ([_colUtils randomFloatBetweenZeroAndOne] - 0.5f) * width);
}

/**
 *
 *  Sets a random velocity of the object and updates the corresponding parameters.
 *
 *  @param width The width of the box.
 *
 */
- (void)setRandomVelocityInBoxWithWidth:(CGFloat)width
{
	_translVelocity = GLKVector3Make([_colUtils randomFloatBetweenZeroAndOne] - 0.5f,
                                     [_colUtils randomFloatBetweenZeroAndOne] - 0.5f,
                                     [_colUtils randomFloatBetweenZeroAndOne] - 0.5f);
    _translVelocity = GLKVector3MultiplyScalar(_translVelocity, width / 20.0f);
	_rotAxis = GLKVector3Normalize(GLKVector3Make([_colUtils randomFloatBetweenZeroAndOne] - 0.5f,
                                                  [_colUtils randomFloatBetweenZeroAndOne] - 0.5f,
                                                  [_colUtils randomFloatBetweenZeroAndOne] - 0.5f));
	_rotAngle = 6.5f * [_colUtils randomFloatBetweenZeroAndOne];
    _rotVelocity = GLKMatrix4MakeRotation(GLKMathDegreesToRadians(_rotAngle), _rotAxis.x, _rotAxis.y, _rotAxis.z);
}

/**
 *
 *  Use this method to invert the rotation and translation velocities. _(Example: When a collision occurs.)_
 *
 */
- (void)invertVelocity
{
    // Invert the velocity of the object
    // Calculate the new velocity of the object by calculating the reflection vector given the translation velocity and the collision normal
    //
    GLKVector3 vectorRight = GLKVector3MultiplyScalar(_collisionNormal, 2.0f * GLKVector3DotProduct(_translVelocity, _collisionNormal));
    _translVelocity = GLKVector3Subtract(_translVelocity, vectorRight);
    
    // Invert the rotation angle
    _rotAngle = -_rotAngle;
    _rotVelocity = GLKMatrix4MakeRotation(GLKMathDegreesToRadians(_rotAngle), _rotAxis.x, _rotAxis.y, _rotAxis.z);
}

/**
 *
 *  Sets the collision normal of the object and sets the _collision_ parameter to _YES_.
 *
 *  @param collisionNormal The hit normal.
 *  @param hitFrame The frame number when the object was hit.
 *
 */
- (void)setCollisionNormal:(GLKVector3)collisionNormal forHitFrame:(unsigned int)hitFrame
{
    _hitFrame = hitFrame;
	_collisionNormal = collisionNormal;
}

/** @name Texture handling */

/**
 *
 *  Binds a texture to the object.
 * 
 *  @param filename The filename of the texture image.
 *  @param textureType The type of the texture to be loaded. (currently supporting only _PVR, PNG and JPEG_ files)
 *  @param optionsDict Dictionary containing the texture options.
 *  @param asynchronously When set to _YES_ the method will load the texture asynchronously.
 *
 */
- (void)setTextureWithFilename:(NSString *)filename ofType:(ColTextureType)textureType options:(NSDictionary *)optionsDict asynchronously:(BOOL)asynchronously cachedFromObject:(CollidableObject *)collObject
{
    if (!collObject) {
        if (asynchronously)
            [_colUtils textureAsynchronouslyFromImageWithFilename:filename
                                                           ofType:textureType
                                                          options:optionsDict
                                                       completion:^(GLKTextureInfo *textureInfo, NSError *outError) {
                                                           _textureInfo = textureInfo;
                                                       }];
        else
            _textureInfo = [_colUtils textureFromImageWithFilename:filename
                                                            ofType:textureType
                                                           options:optionsDict];
    }
    else {
        _textureInfo = collObject.textureInfo;
    }
}

/** @name Geometry */

/**
 *
 *  Loads a custom .obj-file and binds the geometry to the object.
 *
 *  @param geometryType The geometry type we want to load and bind to the geometry of the object.
 *  @return Returns _NO_ if it succeeds and _YES_ otherwise.
 *
 */
- (BOOL)setCustomModelGeometryOfType:(ColGeometryType)geometryType cachedFromObject:(CollidableObject *)collObject
{
    _geometryType = geometryType;
    
    if (!collObject) {
        BOOL result = [_colUtils loadGeometry:&_geometry withGeometryType:geometryType];
        [_colUtils createVertexArray:_vertexArrayObject forVertexBufferObject:_vertexBufferObject forGeometry:&_geometry geometryType:_geometryType];
        return result;
    }
    else {
        _geometry = collObject.geometry;
        _vertexArrayObject = collObject.vertexArrayObject;
        _vertexBufferObject = collObject.vertexBufferObject;
        _indexBufferObject = collObject.indexBufferObject;
        
        return NO;
    }
}

/**
 *
 *  Creates a sphere and binds the geometry to the object.
 *
 *  @param depth The depth of the sphere to be created.
 *  @param radius The radius of the sphere.
 *
 */
- (void)setSphereGeometryWithSphereDepth:(unsigned int)depth andRadius:(REAL)radius cachedFromObject:(CollidableObject *)collObject
{
    _geometryType = ColGeometryTypeOther;
    
    if (!collObject) {
        [_colUtils makeSphere:&_geometry withDepth:depth andRadius:radius];
        [_colUtils createVertexArray:_vertexArrayObject forVertexBufferObject:_vertexBufferObject forGeometry:&_geometry geometryType:_geometryType];
    }
    else {
        _geometry = collObject.geometry;
        _vertexArrayObject = collObject.vertexArrayObject;
        _vertexBufferObject = collObject.vertexBufferObject;
        _indexBufferObject = collObject.indexBufferObject;
    }
}

/**
 *
 *  Creates a cube and binds the geometry to the object.
 *
 *  @param radius The radius of the cube, that is half of the side length.
 *
 */
- (void)setCubeGeometryWithCubeRadius:(REAL)radius cachedFromObject:(CollidableObject *)collObject
{
    _geometryType = ColGeometryTypeOther;
    
    if (!collObject) {
        [_colUtils makeCube:&_geometry withRadius:radius];
        [_colUtils createVertexArray:_vertexArrayObject forVertexBufferObject:_vertexBufferObject forGeometry:&_geometry geometryType:_geometryType];
    }
    else {
        _geometry = collObject.geometry;
        _vertexArrayObject = collObject.vertexArrayObject;
        _vertexBufferObject = collObject.vertexBufferObject;
        _indexBufferObject = collObject.indexBufferObject;
    }
}

/**
 *
 *  Creates a plane and binds the geometry to the object.
 *
 *  @param width The width of the plane.
 *  @param height The height of the plane.
 *  @param vert The vertical height of the plane.
 *  @param hor The horizontal height of the plane.
 *
 */
- (void)setPlaneGeometryWithPlaneWidth:(REAL)width height:(REAL)height atVerticalHeight:(unsigned int)vert andHorizontalHeight:(unsigned int)hor cachedFromObject:(CollidableObject *)collObject
{
    _geometryType = ColGeometryTypeOther;
    
    if (!collObject) {
        [_colUtils makePlane:&_geometry withWidth:width height:height atVerticalHeight:vert andHorizontalHeight:hor];
        [_colUtils createVertexArray:_vertexArrayObject forVertexBufferObject:_vertexBufferObject forGeometry:&_geometry geometryType:_geometryType];
    }
    else {
        _geometry = collObject.geometry;
        _vertexArrayObject = collObject.vertexArrayObject;
        _vertexBufferObject = collObject.vertexBufferObject;
        _indexBufferObject = collObject.indexBufferObject;
    }
}

/** @name Drawing */

/**
 *
 *  Draws the object.
 *
 *  @param mode The mode we will use to draw the geometry, i.e. the mode OpenGL will use to interpret the data (e.g. *GL_TRIANGLES*, *GL_LINES*, etc.)
 *
 */
- (void)drawWithMode:(GLenum)mode
{
    [_baseEffect prepareToDraw];
    glBindVertexArrayOES(_vertexArrayObject);
    [_colUtils drawGeometry:&_geometry withMode:mode geometryType:_geometryType];
}

//
//  Deallocate all resources we allocated previously.
//
- (void)dealloc
{
    [_colUtils deleteVertexArray:_vertexArrayObject forVertexBufferObject:_vertexBufferObject];
}

@end
