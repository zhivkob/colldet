//
//  Constants.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/6/12.
//
//

#import "Constants.h"

/**
 *
 *  Constants we use in the main scene
 *
 */

// Constant we use to filter the acceleration
CGFloat const AccelerationFiltering = 0.05f;

// The position of the eye in the menu scene
GLKVector3 const EyePositionMenu = GLKVector3Make(0.0f, 0.0f, 1.0f);

// The initial translation of the entire scene
GLKMatrix4 TranslationMenu = GLKMatrix4MakeTranslation(0.0f, 0.0f, -1.5f);

// The lookAt-Matrix used in the menu
GLKMatrix4 LookAtMenu = GLKMatrix4MakeLookAt(EyePositionMenu.x, EyePositionMenu.y, EyePositionMenu.z, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

/** 
 *
 *  Constants we use in the first demo
 *
 */

// The space between the balls
CGFloat const SpaceBetweenBalls = 1.5f;

// The balls count in the scene
NSUInteger const BallsCount = 4;

// The space on the left and right sides before the ball hits the wall
CGFloat const BoundsOffset = 1.75f;

// The left bound
CGFloat const LeftBound = -((BallsCount - 1) * SpaceBetweenBalls) / 2.0f - BoundsOffset;

// The right bound
CGFloat const RightBound = ((BallsCount - 1) * SpaceBetweenBalls) / 2.0f + BoundsOffset;

// The radius of the balls
CGFloat const BallRadius = 1.75f;

// The depth (smoothness) of the spheres (balls)
NSUInteger const BallDepth = 1;

// The friction when rotating the vases
CGFloat const RotationFriction = 0.2f;

// The axis the balls use for the rotation
GLKVector3 const RotationAxis = GLKVector3Normalize(GLKVector3Make(0.0f, 1.0f, 0.0f));

// The angle with which the balls rotate every update (in radians)
CGFloat const RotationAngle = GLKMathDegreesToRadians(9.0f);

// The rotation angle with which the scene constantly rotates by itself
CGFloat const SceneRotationAngle = 0.2f;

// The movement speed of the balls
CGFloat const MovementOffset = 0.03f;

// The width of the plane on which the balls are placed
CGFloat const PlaneWidth = 40.0f;

// The height of the plane on which the balls are placed
CGFloat const PlaneHeight = 20.0f;

// Should the balls rotate or not
BOOL const BallsRotate = YES;

// The position of the eye in the first demo
GLKVector3 const EyePosition0 = GLKVector3Make(0.0f, -5.0f, 5.0f);

// The initial translation of the entire scene
GLKMatrix4 TranslationFirstDemo = GLKMatrix4MakeTranslation(0.0f, 0.0f, -10.0f);

// The lookAt-Matrix used in the first demo
GLKMatrix4 LookAtFirstDemo = GLKMatrix4MakeLookAt(EyePosition0.x, EyePosition0.y, EyePosition0.z, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);



/**
 *
 *  Constants we use in the second demo
 *
 */

// The radius of the box the objects are in
CGFloat const CageRadius = 3.0f;

// The number of objects in the box
NSUInteger const ObjectsCount = 6;

// The radius of the objects
CGFloat const ObjectsRadius = 0.7f;

// A constants we use for the accelerometer data calculations (used in the linear interpolation)
CGFloat const InterpolationAlpha = 0.8f;

// The position of the light source in the second demo
GLKVector4 const LightPosition1 = GLKVector4Make(0.0f, 0.0f, 3.0f, 1.0f);

// The position of the eye in the second demo
GLKVector3 const EyePosition1 = GLKVector3Make(0.0f, 1.0f, 2.0f);

// The initial translation of the entire scene
GLKMatrix4 TranslationSecondDemo = GLKMatrix4MakeTranslation(0.0f, -1.0f, -1.5f);

// The lookAt-Matrix used in the second demo
GLKMatrix4 LookAtSecondDemo = GLKMatrix4MakeLookAt(EyePosition1.x, EyePosition1.y, EyePosition1.z, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);



/**
 *
 *  Constants we use in the entire application
 *
 */

// Global constant we use describing the level of accuracy of the detection
col::LevelOfDetectionE const LevelOfDetection = col::LEVEL_EXACT;

// Used to specify a timestamp between two collisions using the frames count
unsigned int const LastCollisionFramesCountThreshold = 5;

// An array containing which buffers should be discarded at the end of each draw
GLenum const discards[] = {
    GL_DEPTH_ATTACHMENT,
    GL_COLOR_ATTACHMENT0
};

// The angle in radians specifying the field of view angle in the y direction
float const FovyRadians = GLKMathDegreesToRadians(75.0f);

// The z-Near plane of the frustum
float const ZNear = 1.0f;

// The z-Far plane of the frustum
float const ZFar = 100.0f;