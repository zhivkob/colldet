//
//  CollidableObject.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "Constants.h"
#import "ColUtilsGLES.h"

@interface CollidableObject : NSObject
{
    // Reference to the collision pipeline
    col::CollisionPipeline *_pipeline;
    
    // Reference to the base effect (integrated shader) we would use
    GLKBaseEffect *_baseEffect;
    
    // Reference to the collision utilities used for creation/destroying of objects, etc.
    ColUtilsGLES *_colUtils;
}

/**
 Returns the _colId_ of the object returned when making the object collidable.
 */
@property (nonatomic, readonly) col::ColID colId;

/**
 Returns a reference to the geometry of the object.
 */
@property (nonatomic, readonly) col::ColGeometry geometry;

/**
 Returns the type of the geometry.
 */
@property (nonatomic, readonly) ColGeometryType geometryType;

/**
 Returns a _GLKTextureInfo_ object containing the texture information.
 */
@property (strong, nonatomic, readonly) GLKTextureInfo *textureInfo;

/**
 Returns the vertex buffer object used for drawing.
 */
@property (nonatomic, readonly) GLuint vertexBufferObject;

/**
 Returns the vertex array object used for drawing.
 */
@property (nonatomic, readonly) GLuint vertexArrayObject;

/**
 Returns the index buffer object used for drawing.
 */
@property (nonatomic, readonly) GLuint indexBufferObject;

/**
 The old matrix of the object.
 */
@property (nonatomic) GLKMatrix4 oldMatrix;

/**
 The current matrix of the object.
 */
@property (nonatomic) GLKMatrix4 currentMatrix;

/**
 The translation velocity of the object.
 */
@property (nonatomic) GLKVector3 translVelocity;

/**
 The rotation velocity of the object.
 */
@property (nonatomic) GLKMatrix4 rotVelocity;

/**
 The rotation axis of the object.
 */
@property (nonatomic) GLKVector3 rotAxis;

/**
 The collision normal of the object.
 */
@property (nonatomic) GLKVector3 collisionNormal;

/**
 The diffuse color of the object.
 */
@property (nonatomic) GLKVector4 diffuseColor;

/**
 The ambient color of the object.
 */
@property (nonatomic) GLKVector4 ambientColor;

/**
 The specular color of the object.
 */
@property (nonatomic) GLKVector4 specularColor;

/**
 The shininess of the object.
 */
@property (nonatomic) float shininess;

/**
 The rotation angle of the object.
 */
@property (nonatomic) double rotAngle;

/**
 Indicates if the object collided previous frame.
 */
@property (nonatomic) BOOL isColliding;

/**
 The frame when the object got hit.
 */
@property (nonatomic) unsigned int hitFrame;

- (id)initWithPipeline:(col::CollisionPipeline *)pipl baseEffect:(GLKBaseEffect  *)baseEffect;

- (void)makeCollidable;
- (void)updatePipeline;
- (void)setCollisionNormal:(GLKVector3)collisionNormal forHitFrame:(unsigned int)hitFrame;

// Binds a texture to the object with the specified name
- (void)setTextureWithFilename:(NSString *)filename ofType:(ColTextureType)textureType options:(NSDictionary *)optionsDict asynchronously:(BOOL)asynchronously cachedFromObject:(CollidableObject *)collObject;

// Set geometry properties of the object
- (BOOL)setCustomModelGeometryOfType:(ColGeometryType)geometryType cachedFromObject:(CollidableObject *)collObject;
- (void)setSphereGeometryWithSphereDepth:(unsigned int)depth andRadius:(REAL)radius cachedFromObject:(CollidableObject *)collObject;
- (void)setCubeGeometryWithCubeRadius:(REAL)radius cachedFromObject:(CollidableObject *)collObject;
- (void)setPlaneGeometryWithPlaneWidth:(REAL)width height:(REAL)height atVerticalHeight:(unsigned int)vert andHorizontalHeight:(unsigned int)hor cachedFromObject:(CollidableObject *)collObject;

- (void)drawWithMode:(GLenum)mode;

// Set color properties of the object
- (void)setRandomColor;
- (void)setRandomPositionInBoxWithWidth:(CGFloat)width;
- (void)setRandomVelocityInBoxWithWidth:(CGFloat)width;
- (void)invertVelocity;

@end
