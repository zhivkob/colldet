
/*****************************************************************************\
 *                           Various "small" classes
\*****************************************************************************/

/*! @file 
 *
 * @brief
 *    Infrastructure for implementing the collision detection pipeline.
 *
 *  Classes for storing various (possibly intermediate) information
 *  about objects and collisions.
 *
 *  For each object that is registered with the collision detection module
 *  a ColObj is created. This instance holds a pointer to the geometry
 *  plus various flags and auxiliary data like the convex hull, connectivity
 *  data structures, Boxtree, Doptree, etc.
 *
 *  Two ColObj's make a ColPair. Lists of such ColPair's are passed down the
 *  collision detection pipeline thereby filtering these lists.
 *
 *  For an extensive explanation of the collision detection pipeline, please see
 *  my dissertation at http://www.gabrielzachmann.org/ .
 *
 * @implementation
 *   A word about exceptions: I have used exceptions, in particular in
 *   constructors. However, the application programmer should not need
 *   to catch exceptions, because all of them are caught by the API
 *   (at least that's the idea). One reason for this was that the app.
 *   programmer won't see any exceptions anyway, if the collision
 *   detection module runs in its own thread (I think).
 *
 * @author Gabriel Zachmann
 *
 * @todo
 * - Flags @a m_stationary and @a m_flexible (siehe ctor) auswerten.
 * - Die Instanzvariable @a m_name im @a ColObj zu String machen.
 * - Internetadresse in Kommentaren anpassen
 *  
 */


//---------------------------------------------------------------------------
//  Includes
//---------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <algorithm>

#define COL_EXPORT

#include <ColObj.h>
#include <ColUtils.h>
#include <ColDefs.h>


namespace col {


//**************************************************************************
// ColObj
//**************************************************************************


/** @class ColObj
 *
 * One collidable object
 *
 * Holds various flags for one collidable object, including
 * all auxiliary collision detection data (hierarchies, etc.).
 *
 * @todo
 *   Was man noch tun muss ..
 *
 * @implementation
 *   Each ColObj stores the toWorld matrix of the geometry,
 *   because the collision detection module does not necessarily run
 *   in its own thread, and thus does not necessarily have its own aspect.
 *
 *
 **/

//---------------------------------------------------------------------------
//  Public Instance Methods
//---------------------------------------------------------------------------


// --------------------------------------------------------------------------
/** @name               Constructors, desctructors, assignment
 */
// @{

/** The default ctor is not meant for "real" usage
 * It is only there so that we can create vector's.
 */

ColObj::ColObj()
:	
	m_objID(0),
	m_geom(NULL),
	m_old_matr(NULL),
	m_cur_matr(NULL),
	m_active(false),
	m_flexible(true),
	m_stationary(false),
	m_doptree(NULL),
	m_boxtree(NULL),
	//m_name(NULL),
	m_col_matr_idx(-1),
	m_cycle(UINT_MAX-17),
	m_has_moved(true),
	m_hull(),
    m_gridobj()
{
}



/** The ctor you should use.
 *
 * @param geom,node		the OSG object
 * @param flexible		tells the coll.det. module that this object might deform
 * @param stationary	tells the coll.det. module that this object won't move
 * @param compute_hull	a convex hull of @a m_geom will be computed and stored,
 *                      
 * @param algo			determines what hierarchical data structure to build
 * @param show_hull		creates a geometry from the convex hull
 * @param colname		name of colobj
 *
 * This is the ctor whould should be used for creating ColObj's.
 *
 * If an object is not m_stationary, then hasMoved() will return true
 * when called for the first time (whether or not the object really has moved).
 *
 * If @a show_hull is @c true, then the convex hull geometry will be attached
 * to the @a m_node so that it will get rendered, too.
 *
 * @a Colname is the name which will be printed with ColObj::print;
 * if it is NULL, then the OSG name will be printed; if that does not exist,
 * an automatically generated ID will be printed.
 *
 * @pre
 *   @a m_geom should belong to @a m_node.
 *
 * @throw XDopTree
 *   If geometry has no polygons, or no GeoPosition3f.
 * @throw XColBug
 *   If a bug in the doptree code occurs.
 * @throw bad_alloc

 * @todo
 *   Parameter @a m_geom ist ueberfluessig.
 *
 * @see
 *   MatrixCell::check
 */
ColObj::ColObj( const ColID				objID,
				const ColGeometry		*geom,
				bool					flexible,
				bool					stationary,
				bool					compute_hull,
				AlgoE					algo,
				Grid					*grid,
				bool					show_hull /*= false*/ )
:	
	m_objID(objID),
	m_geom(geom),
	m_old_matr(NULL),
	m_cur_matr(NULL),
	m_active(true),
	m_flexible(flexible),
	m_stationary(stationary),
	m_doptree(NULL),
	m_boxtree(NULL),
	m_col_matr_idx(-1),
	m_cycle(UINT_MAX-17),
	m_has_moved(false),
	m_first_moved_check(!stationary),
	m_hull(),
	m_gridobj()
{
	// m_cur_matr shouldn't be NULL
	m_cur_matr = new Matrix4( Matrix4::identity() );

	if ( algo == ALGO_DOPTREE )
		m_doptree = new DopTree( m_geom, points);
	else
	if ( algo == ALGO_BOXTREE )
		m_boxtree = new Boxtree( m_geom, points);
	else
		throw XColBug("ColObj: algo (%d) unknown", algo );

	// set name

	// construct convex hull
	if ( compute_hull )
		try
		{
			m_hull = m_geom;
		}
		catch ( XCollision &x )
		{
			fprintf(stderr,"\ncol:ColObj: computing the convex hull of "
					"%d failed!\n"
					" Will not use convex hull pre-checks.\n\n",
					m_objID );
			x.print();
		}

	// show hull, if wanted
	if ( show_hull && compute_hull )
	{
		// todo
	}
	
	// construct gridobj
	if ( grid != NULL )
	{
		// todo
		//
		REAL min[3];
		REAL max[3];

		m_geom->getBBox( min, max );
		m_gridobj = new GridObj( grid, this, min, max );
	}
	else
		 m_gridobj = NULL;
}


ColObj::~ColObj() throw()
{
	/* the resource, which are created by collisionpipeline,
	 * will be released here
	 */
	if ( m_cur_matr != NULL )
		delete m_cur_matr;
	if ( m_gridobj != NULL )
		delete m_gridobj;
	if ( m_doptree != NULL )
		delete m_doptree;
	if ( m_boxtree != NULL )
		delete m_boxtree;
}



/** Copy a collision object
 * @warning
 *   Since the two colobj's point to the same geometry,
 *   their DOP trees are copied @e only shallow!
 */

ColObj::ColObj( const ColObj &source )
:	m_objID(source.m_objID),
	m_active(source.m_active),
	m_flexible(source.m_flexible),
	m_stationary(source.m_stationary),
	m_doptree(source.m_doptree),
	m_boxtree(source.m_boxtree),
	m_geom(source.m_geom),
	m_old_matr(source.m_old_matr),
	m_cur_matr(source.m_cur_matr),
	m_col_matr_idx(source.m_col_matr_idx),
	m_cycle(source.m_cycle),
	m_has_moved(source.m_has_moved),
	m_first_moved_check(source.m_first_moved_check),
	m_hull(source.m_hull),
    m_gridobj(source.m_gridobj)
{
	if ( m_gridobj )
		m_gridobj->setColObj(this);
}


/** Assignment
 * @warning
 *   Since the two colobj's point to the same geometry,
 *   their DOP trees are copied @e only shallow!
 */

void ColObj::operator = ( const ColObj &source )
{
	if ( this == &source )
		return;

	m_active = source.m_active;
	m_flexible = source.m_flexible;
	m_stationary = source.m_stationary;
	m_doptree = source.m_doptree;
	m_boxtree = source.m_boxtree;
	m_geom = source.m_geom;
	m_old_matr = source.m_old_matr;
	m_cur_matr = source.m_cur_matr;
	m_col_matr_idx = source.m_col_matr_idx;
	m_objID = source.m_objID;
	m_cycle = source.m_cycle;
	m_has_moved = source.m_has_moved;
	m_first_moved_check = source.m_first_moved_check;
	m_hull = source.m_hull;
	m_gridobj = source.m_gridobj;
	if(m_gridobj)
		m_gridobj->setColObj(this);
}



// --------------------------------------------------------------------------
// @}
/** @name               Checks
 */
// @{

/** 
 * @brief 
 *   set the colobj's current transformation matrix
 * 
 * @param tran pointer to the const matrix, which is created by pipeline
 * 
 * @return 
 */
bool ColObj::setTransMatrix( const Matrix4 *tran ) 
{
	if (m_old_matr != NULL)
	{
		// the matrix is created by collisionPipiline.
		// but pipeline dosen't know whether it 's already useless or not
		// has to be deleted here.
		delete m_old_matr;
		m_old_matr = m_cur_matr; 
		m_cur_matr = tran; 
		m_has_moved =  ! equal( *m_old_matr, *m_cur_matr, NearZero*20 );
	}
	else
	{
		m_old_matr = m_cur_matr; 
		m_cur_matr = tran; 
		m_has_moved = true;
	}

	return m_has_moved;
}

/**  Update current world bbox 
 *
 * Calculates the current bbox in world coordinates.
 * 
 * By Collision::check, we can confirm that only moved colobjs will call this function
 *

 **/

void ColObj::updateBBox( void )
{
	Point3 bmin, bmax;
	m_geom->getBBox( bmin, bmax );

	Point3 bbox[8];
	bbox[0] = Point3( bmin[0], bmin[1], bmin[2] );
	bbox[1] = Point3( bmin[0], bmin[1], bmax[2] );
	bbox[2] = Point3( bmax[0], bmin[1], bmax[2] );
	bbox[3] = Point3( bmax[0], bmin[1], bmin[2] );
	bbox[4] = Point3( bmin[0], bmax[1], bmin[2] );
	bbox[5] = Point3( bmin[0], bmax[1], bmax[2] );
	bbox[6] = Point3( bmax[0], bmax[1], bmax[2] );
	bbox[7] = Point3( bmax[0], bmax[1], bmin[2] );

	for ( unsigned int i = 0; i < 8; i ++ )
	{
		Vector4 v4 = (*m_cur_matr) * bbox[i];
		bbox[i][0] = v4[0];
		bbox[i][1] = v4[1];
		bbox[i][2] = v4[2];
	}

	m_bmin[0] =
	m_bmin[1] = 
	m_bmin[2] = FLT_MAX;
	m_bmax[0] =
	m_bmax[1] = 
	m_bmax[2] = FLT_MIN;

	for ( unsigned int i = 0; i < 8; i ++ )
	{
		for ( int j=0; j<3; ++j )
		{
			if ( bbox[i][j] < m_bmin[j] )
			{
				m_bmin[j] = bbox[i][j];
			}
			if ( bbox[i][j] > m_bmax[j] )
			{
				m_bmax[j] = bbox[i][j];
			}
		}
	}
}



/**  Check if the bboxes of two objects intersect
 *
 * @param other		another collision object
 *
 **/

bool ColObj::bboxIntersects( ColObj* other )
{
	updateBBox();
	other->updateBBox();

	//bbox intersect test
	if ( other->m_bmin[0] <= m_bmax[0] &&  // if other->bmin <= bmax
		 other->m_bmin[1] <= m_bmax[1] &&
		 other->m_bmin[2] <= m_bmax[2] )
	{
		if ( other->m_bmin[0] >= m_bmin[0] &&  // if other->bmin >= bmin
			 other->m_bmin[1] >= m_bmin[1] &&
			 other->m_bmin[2] >= m_bmin[2] )
			return true;
		else if ( other->m_bmax[0] >= m_bmin[0] && // else if other->bmax >= bmin
				  other->m_bmax[1] >= m_bmin[1] &&
				  other->m_bmax[2] >= m_bmin[2] )
			return true;	
	}
	return false;
}



/**  Update toworld matrix and check whether or not an object has moved
 *
 * This is based on a toWorld matrix comparison.
 * The check will be performed only once per collision cycle.
 *
 * Whether or not the obj has moved, @a m_curr_matr will be copied into
 * m_old_matr, and @a m_curr_matr will get the node's current toWorld matrix.
 * This happens only if @a global_cycle has been incremented.
 *
 * @see
 *   MatrixCell::check()
 **/
bool ColObj::hasMoved( unsigned int global_cycle )
{
	m_cycle = global_cycle;
	if ( m_cur_matr == NULL )
	{
		// The colobj hasn't been connected with a trans matrix
		return false;
	}
	return m_has_moved;
}


/**  Set the status of the object as moved. Needed for forceCheck
 *
 * This is based on a toWorld matrix comparison.
 * The check will be performed only once per collision cycle.
 *
 * Whether or not the obj has moved, @a m_curr_matr will be copied into
 * m_old_matr, and @a m_curr_matr will get the node's current toWorld matrix.
 * This happens only if @a global_cycle has been incremented.
 *
 * @see
 *   MatrixCell::check()
 **/
void ColObj::setMoved( unsigned int global_cycle )
{
	m_cycle = global_cycle;
	if ( m_cur_matr != NULL )
		m_has_moved = true;
}


/**  Update GridObj if object has moved
*
 **/
void ColObj::updateGrid ( void )
{
	// must get the world-coordinate volume
	// m_geom->getBBox( min, max ) gets the only local-coordinate volume
	
	updateBBox();
	m_gridobj->moveTo( m_bmin, m_bmax );
}


/** Return GridObj
*
**/
GridObj * ColObj::GetGridObj( void )
{
    return m_gridobj;
}

// --------------------------------------------------------------------------
// @}


const char * ColObj::getName( void ) const
{
	static const char *noname = "<noname>";
	return noname;
}

ColID ColObj::getID( void ) const
{
	return m_objID;
}


/** Set the activity-status of a Colobj
 * Used to remove an object temporarily from collision checks
 * @param active     true => object will be checked
 */
void ColObj::setActive( bool active )
{
    m_active = active;
}


/** Returns the activity-status of a Colobj
 */
bool ColObj::isActive() const
{
    return m_active;
}


//---------------------------------------------------------------------------
//  Class methods
//---------------------------------------------------------------------------


/** Find the @a colobjs[i] for which @a colobjs[i].m_node == @a obj;
 *  If not found, returns NULL.
 */

ColObj* ColObj::find( vector<ColObj*> *colobjs, const ColID objID )
{
	for ( unsigned int i = 0; i < colobjs->size(); i ++ )
		if ( (*colobjs)[i]->m_objID == objID )
			return (*colobjs)[i];
	return NULL;
}


//**************************************************************************
// ColPair
//**************************************************************************


/** @class ColPair
 *
 * Pairs of ColPObjs's.
 *
 * This class is mainly useful for making vectors of colobj pairs.
 *
 * @see
 *   ColObj
 *
 * @implementation
 *   Implementierungsdetails, TODO's, etc.
 *
 *
 **/

ColPair::ColPair()
:	m_p(NULL), m_q(NULL)
{ }


ColPair::ColPair( ColObj* colobj_p, ColObj* colobj_q )
:	m_p(colobj_p), m_q(colobj_q)
{ }



ColPair::ColPair( const ColPair &source )
:	m_p(source.m_p), m_q(source.m_q)
{ }


void ColPair::operator =( const ColPair &source )
{
	m_p = source.m_p;
	m_q = source.m_q;
}



ColPair::~ColPair() throw()
{
}


/**  Return one of the two objects of the pair.
 *
 **/

ColObj* ColPair::p( void ) const
{
	return m_p;
}

/**  Return the other one of the two objects of the pair.
 *
 **/

ColObj* ColPair::q( void ) const
{
	return m_q;
}



//**************************************************************************
// MatrixCell
//**************************************************************************


/** @class MatrixCell
 *
 *  @brief A single cell of the collision interest matrix.
 *
 * Each cell contains a list of Callback's, and other pairwise data
 * (like separating plane).
 *
 * Each cell also contains a "level of collision".
 * The minimum level is @a LEVEL_BOX.
 * When a cell checks the pair of objects for collision, the maximum level
 * of all callbacks is used for that check.
 *
 * @throw XCollision
 *   If one of the nodes does not have a geometry.
 *
 * @todo
 *   Flag @a all_poygons auswerten.
 *
 * @author Gabriel Zachmann
 *
 * @implementation
 *   When different algorithms will be available, a cell will be the place to
 *   store the kind of algo appropriate for a certain pair of objects.
 */
MatrixCell::MatrixCell( const ColObj *colobj1, const ColObj *colobj2 )
:	
	m_callback(),
	m_colobj1(colobj1), m_colobj2(colobj2),
	m_level(LEVEL_BOX),
	m_sep_plane(),
	m_data( colobj1->m_geom, colobj2->m_geom ),
    m_allpolygons(false)
{ }



/** Add a collision callback
 * @throw XColBug
 *   If m_callback->m_node1/2 doesn't match cell.m_colobj1/2->m_node.
 * @throw XCollision
 *   If one of the objects pointers in the callback is a NullNode.
 */
void MatrixCell::addCallback( Callback *cb )
{
	if ( cb->obj1ID < 0 || cb->obj2ID < 0 )
		throw XCollision("MatrixCell:addCallback: obj1 or obj2 is a NullNode");
	
	// todo...
	if ( cb->obj1ID != m_colobj1->m_objID ||
		 cb->obj2ID != m_colobj2->m_objID  )
	// the colobj must be identified with something( int? char? )
		throw XColBug("MatrixCell:addCallback: m_callback->m_node1/2 doesn't"
					  " match m_colobj1/2->m_node");

	m_callback.push_back( cb );

	if ( cb->level > m_level )
		m_level = cb->level;

    if ( cb->all_polygons )
        m_allpolygons = true;
}


/** Process all callbacks
 * @pre
 *   @a m_data is valid.
 */
void MatrixCell::callCallbacks( void ) const
{
	for ( unsigned int i = 0; i < m_callback.size(); i ++ )
		(*m_callback[i])( &m_data );
}


/** Check a pair for collision (internal)
 *
 * @param use_hulls		do a convex hull pre-check
 *
 * Depending on the levels of detection of each callback, the max level
 * needed is done. For instance, if all callbacks have level LEVEL_HULL or less,
 * then only the convex hull check is done.
 *
 * @implementation
 *   The check is based on the positions of the objects stored
 *   in ColObj::m_curr_matr .
 *
 * @warning
 *   Only one of the instance variables @c m_doptree and @c m_boxtree
 *   should be set! It will call the check() function of the one which is set.
 *   And both ColObj's in a cell should have the same instance variables set,
 *   and the other unset!
 *
 * @see
 *   ColObj::hasMoved()
 *
 * @todo
 * - Check whether or not only a bbox check is wanted. This would be a flag
 *   stored with each Callback, and a counter stored with the MatrixCell.
 * - Matrix-Inversion in ColObj::hasMoved() machen.
 * - Nochmal ueberpruefen, warum die berechnete Matrix m12 so stimmt;
 *   eigtl. haette ich jetzt doch eine umgekehrte Multiplikation erwartet.
 * - @a use_hulls in jeder MatrixCell speichern. Dann braucht man nicht das 
 *   Flag global fuer alle MatrixCell's in Collision.cpp sich zu merken.
 */

bool MatrixCell::check( bool use_hulls, bool verb_print )
{
	// compute transformation matrix from obj1 into obj2
	m_data.m12 = *( m_colobj2->m_cur_matr );
	m_data.m12 = inverse( m_data.m12 );
	m_data.m12 = m_data.m12 * *( m_colobj1->m_cur_matr );

    // debug...
	//m_data.m12 = *m_colobj2->m_cur_matr;
	//m_data.m12 = inverse( m_data.m12 );
	//m_data.m12 = m_data.m12 * ( *m_colobj1->m_cur_matr );
	
    m_data.all_polygons = m_allpolygons; 

	// do the convex hull pre-check
	if ( use_hulls )
	{
		bool hull_coll = m_colobj1->m_hull.check( m_colobj2->m_hull,
											  m_data.m12, &m_sep_plane );
		if ( m_level <= LEVEL_HULL || ! hull_coll )
		{	
			if ( verb_print )
				printf("The ConvexHull is NOT intersected\n");
			return hull_coll;
		}
	}

	// do an exact check
    m_data.polisecdata.clear();

	if ( m_colobj1->m_doptree )
		m_colobj1->m_doptree->check( *m_colobj2->m_doptree, &m_data );
	else
	if ( m_colobj1->m_boxtree )
		m_colobj1->m_boxtree->check( *m_colobj2->m_boxtree,
									 m_colobj1->m_geom,
									 m_colobj2->m_geom,
									 &m_data );
	else
		throw XColBug("MatrixCell::check: algo unknown");

    if ( m_data.polisecdata.empty() )
        return false;

    return true;
}


//**************************************************************************
// Matrix
//**************************************************************************


/** @class Matrix
 *    The collision interest matrix.
 *
 *  Composed of MatrixCell's.
 *
 *  @author Gabriel Zachmann
 */



/**  Create a collision interest matrix 
 *
 * @param numcolobjs	this is just an estimate of how many objects there will be
 *
 * The number of collision objects can be incremented later.
 *
 * @implementation
 *   Only the lower triangle of the matrix is occupied,
 *   i.e., only cells (i,j) are valid with i>j.
 *
 **/

Matrix::Matrix( unsigned int numcolobjs /* = 50 */ )
:	m_m()
{
	m_m.reserve( numcolobjs );
}



/**  Make a new row & column for a collision object
 *
 * @param obj	the collision object
 *
 * A row is added to the matrix.
 *
 * @throw XColBug
 *   If @a colobj has already been added.
 *
 * @warning
 *   Dinge, die der Aufrufer unbedingt beachten muss...
 *
 * @pre
 *   Annahmnen, die die Funktion macht...
 *
 * @sideeffects
 *   Nebenwirkungen, globale Variablen, die veraendert werden, ..
 *
 * @todo
 *   Was noch getan werden muss
 *
 * @bug
 *   Bekannte Bugs dieser Funktion
 *
 * @see
 *   ...
 *
 * @implementation
 *   Implementierungsdetails, TODOs, ...
 *
 **/

void Matrix::addObj( ColObj *obj )
{
	if ( obj->m_col_matr_idx >= 0 )
		throw XColBug("Matrix::addObj: ColObj has been added already");

	obj->m_col_matr_idx = m_m.size();

	m_m.push_back( m_Row( m_m.size(), NULL ) );
}



/**  Add a callback to a cell of the matrix
 *
 * @param callback		the callback
 *
 * @throw XCollision
 *   If the same @a m_callback, or a callback with the same objects,
 *   has been added already.
 * @throw XCollision
 *   If the nodes have not been made collidable yet (by an ADD_OBJECT request).
 * @throw XColBug
 *
 * @warning
 *   A pointer to the callback-functor is stored, so the application
 *   should not delete the object.
 *
 * @pre
 *   Annahmnen, die die Funktion macht...
 *
 * @todo
 *
 * @implementation
 *   We cannot check earlier whether or not the nodes have been made collidable,
 *   because that could have happened by just one request earlier in the
 *   same queue during the same collision cycle.
 *
 **/

void Matrix::addCallback( Callback *callback, vector<ColObj*> *colobjs )
{
	ColObj *obj1 = ColObj::find( colobjs, callback->obj1ID );
	ColObj *obj2 = ColObj::find( colobjs, callback->obj2ID );

	if ( ! obj1 || ! obj2 )
		throw XCollision("Matrix:addCallback: one or both objects of the "
						 "callback are not collidable yet");

	MatrixCell *cell = getCell( ColPair(obj1, obj2) );

	if ( ! cell )
		cell = createCell( obj1, obj2 );

	cell->addCallback( callback );
}


/**  Return the cell corresponding to a colobj pair
 *
 * @param pair		the pair of collision objects
 *
 * @return
 *   The corresponding cell.
 *
 * @throw XColBug
 *   If either of the colmatrix indices is < 0.
 *
 * @pre
 *   - Matrix index of both @a obj @e must be valid.
 *   - obj1 != obj2, and index1 != index2.
 *
 **/

MatrixCell * Matrix::getCell( const ColPair &pair ) const
{
	int i1 = pair.p()->m_col_matr_idx;
	int i2 = pair.q()->m_col_matr_idx;

	if ( i1 < 0 || i2 < 0 )
		throw XColBug("Matrix:getCell: i1 or i2 < 0");

	if ( i1 < i2 )
		swap(i1,i2);
	return m_m[i1][i2];
}



/**  Call all callbacks associated with a certain pair of col. objects
 *
 * @param pair		the pair of col. objects
 *
 * If there are no callbacks associated with this pair, nothing happens.
 *
 * @warning
 *   Dinge, die der Aufrufer unbedingt beachten muss...
 *
 * @pre
 *   - Matrix index of both @a obj @e must be valid.
 *   - obj1 != obj2, and index1 != index2.
 *
 * @sideeffects
 *   Nebenwirkungen, globale Variablen, die veraendert werden, ..
 *
 * @todo
 *   Was noch getan werden muss
 *
 * @bug
 *   Bekannte Bugs dieser Funktion
 *
 * @see
 *   ...
 *
 * @implementation
 *   Implementierungsdetails, TODOs, ...
 *
 **/

void Matrix::callCallbacks( const ColPair &pair ) const
{
	const MatrixCell *cell = getCell( pair );
	if ( ! cell )
		return;

	cell->callCallbacks();
}



/**  Create a new collision interest matrix cell
 *
 * @param obj1,obj2		the pair of collision objects
 *
 * @return
 *   The corresponding cell.
 *
 * @throw XColBug
 *   If either of the colmatrix indices is < 0.
 *
 * @pre
 *   - Matrix index of both @a obj @e must be valid.
 *   - obj1 != obj2, and index1 != index2.
 *
 **/

MatrixCell * Matrix::createCell( const ColObj *obj1, const ColObj *obj2 )
{
	int i1 = obj1->m_col_matr_idx;
	int i2 = obj2->m_col_matr_idx;

	if ( i1 < 0 || i2 < 0 )
		throw XColBug("Matrix:getCell: i1 or i2 < 0");

	if ( i1 < i2 )
		swap(i1,i2);

	m_m[i1][i2] = new MatrixCell( obj1, obj2 );

	return m_m[i1][i2];
}



/** Check a pair for collision
 * @see
 *   MatrixCell::check()
 */

bool Matrix::check( const ColPair &pair, bool use_hulls, AlgoE /*algo*/, bool verb_print ) const
{
	MatrixCell *cell = getCell( pair );
	if ( ! cell )
		return false;

	return cell->check( use_hulls, verb_print );
}





/**  Check consistency of the collision interest matrix
 *
 * @param colobjs 	the list of collision objects
 *
 * @return
 *   True, if an inconsistency has been detected.
 *
 * If an inconsistency is detected, an error message is printed.
 * Checks:
 * -  strict lower triangle;
 * -  @a colobjs must fit to the matrix
 *
 * @pre
 *
 * @implementation
 *   Implementierungsdetails, TODOs, ...
 *
 **/

bool Matrix::isConsistent( vector<ColObj> *colobjs ) const
{
	bool err = false;

	for ( unsigned int i = 0; i < m_m.size(); i ++ )
		if ( m_m[i].size() != i )
		{
			fprintf(stderr,"Matrix: inconsistency!\n"
					"  matrix[%u].size (%zu) != row index %d!\n",
					i, m_m[i].size(), i );
			err = true;
		}

	for ( unsigned int i = 0; i < colobjs->size(); i ++ )
		if ( (*colobjs)[i].m_col_matr_idx < 0 ||
			 (*colobjs)[i].m_col_matr_idx >= static_cast<int>( m_m.size() ) )
		{
			fprintf(stderr,"Matrix: inconsistency!\n"
					"  colobjs[%u].matrix_index (%d) out of range (0..%zu)!\n",
					i, (*colobjs)[i].m_col_matr_idx, m_m.size() );
			err = true;
		}

	return err;
}


}// namespace col

