//---------------------------------------------------------------------------
//  Request
//---------------------------------------------------------------------------
//  Copyright (C):
//---------------------------------------------------------------------------


#ifndef ColRequest_H
#define ColRequest_H
#if defined(__sgi) || defined(_WIN32)
#pragma once
#endif


//---------------------------------------------------------------------------
//  Includes
//---------------------------------------------------------------------------

#include <vector>

#include <col_import_export.h>
#include <Collision.h>
#include <ColGeometry.h>

// Collision detection namespace
namespace col {
    
//---------------------------------------------------------------------------
//  Forward References
//---------------------------------------------------------------------------

struct Data;
struct Dop;
struct PipelineData;

template<class T> class Queue;
struct Request;
class Matrix;
class ColObj;
struct Callback;

/** The types of requests (besides check()) to the collision detection module
 * @warning
 *   If you change this, you @e must change Request::Names!
 */

enum RequestE
{
	// Operations on geometry
	ADD_OBJECT,
	REMOVE_OBJECT,
	ACTIVATE_OBJECT,
	DEACTIVATE_OBJECT,
	MOVE_OBJECT,
	// Operations on callback
	ADD_CALLBACK,
	REMOVE_CALLBACK,
	ADD_CYCLE_CALLBACK
};


/** Each request from the application is encapsulated by an instance of this class
 *
 * In order for the CollisionPipeline to be able to run in parallel to the main
 * ailpcation, requests (such as "register an object") must be queued. This class
 * aides that.
 */

struct COL_EXPORTIMPORT  Request
{
	RequestE			req;						// the raison d'etre
	ColID				objID;
	const ColGeometry	*geom;
	const Matrix4   	*tran;
	Callback			*callback;
	static const char	*Names[];

	Request( RequestE, ColID );
	Request( RequestE, ColID, const ColGeometry* );
	Request( RequestE, ColID, const Matrix4* );
	Request( RequestE, Callback* );

	void operator = ( const Request &source );

	void process( bool show_hulls, AlgoE algo, Matrix* colmatrix,
				  std::vector<ColObj*>* colobjs, std::vector<Callback*> &cycle_callbacks,
				  bool useHulls, Grid* grid );

	const char * getName( void ) const;
};


}//namespace col

#endif //ColRequest_H
