//
//  Scene.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import <ColGeometry.h>
#import <ColTopology.h>
#import <Collision.h>
#import <ColUtils.h>
#import "AudioController.h"
#import "CollidableObject.h"

typedef void (^CollisionBlock)(CollidableObject *collObject1, CollidableObject *collObject2);

/**
 *
 *  The collision callback class which is responsible for the callbacks when 2 objects collide. It triggers an Objective-C block containing both objects as parameters. The user has the freedom to do what they please to with those objects regarding the properties and the methods they have implemented in the class representing that object.
 *
 */
class CollisionCallback : public col::Callback
{
public:
    CollisionCallback(CollidableObject *o1, CollidableObject *o2, col::LevelOfDetectionE level) : col::Callback(o1.colId, o2.colId, false, true, level), _object1(o1), _object2(o2) {
        // Some custom initialization
    };
    virtual ~CollisionCallback() { };
    virtual void operator () (const col::Data *data) throw () {
        // Execute the block when two objects collide
        if (collisionBlock)
            collisionBlock(_object1, _object2);
    };
    
    // The collision block that is to be executed
    CollisionBlock collisionBlock;
protected:
    CollidableObject *_object1;
    CollidableObject *_object2;
};

/**
 *
 *  A protocol describing a Scene. The required methods are the absolute minimum to get a simple OpenGL scene running. The methods setupGL and tearDownGL can be implemented in the init or dealloc routine, but as a convenience they are expected to be implemented as separate methods.
 *
 */
@protocol Scene <NSObject>

@required

/**
 *
 *  Initialization method.
 *
 */
- (id)init;

/**
 *
 *  This method gets called each time when the scene has to be updated.
 *
 */
- (void)update;

/**
 *
 *  This method gets called each time the scene has to be drawn.
 *
 */
- (void)draw;
 
@optional

/**
 *
 *  This method can be called to setup the OpenGL scene.
 *
 */
- (void)setupGL;

/**
 *
 *  This method can be called to tear down the OpenGL scene.
 *
 */
- (void)tearDownGL;

/**
 *
 *  This method can be used to setup the collidable objects in the scene.
 *
 */
- (void)setupCollidableObjects;

/**
 *
 *  This method can be used to move the objects around.
 *
 */
- (void)moveObjects;

/**
 *
 *  This method can be used to perform the checks for collision.
 *
 */
- (void)checkForCollisions;

@end
