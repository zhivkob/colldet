//
//  MenuScene.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import <CoreMotion/CoreMotion.h>
#import "MenuScene.h"
#import "ColUtilsGLES.h"

@interface MenuScene()
{
    // Objects that we will need for the collision detection
    col::CollisionPipeline *_pipeline;
    
    // Base effect (mimics shaders)
    GLKBaseEffect *_baseEffect;
        
    // Variables we will need to present the ground plane to the screen
    CollidableObject *_sceneCage;
    CollidableObject *_ballObject;
    CollidableObject *_cubeObject;
    CollidableObject *_tableObject;
    
    // Global variable for the GL utility methods
    ColUtilsGLES *_colUtils;
    
    // The modelview and projection matrices
    GLKMatrix4 _modelviewMatrix;
    GLKMatrix4 _projectionMatrix;
    
    // The matrix that gets adjusted according to the accelerometer data
    GLKMatrix4 _accelerometerMatrix;
    CGSize _screenSize;
    
    // The shader program
    GLint _program;
    GLint uniforms[NUM_UNIFORMS];
    
    // The audio controller for the scene
    AudioController *_audioController;
    
    // The motion manager
    CMMotionManager *_motionManager;
    
    // The filtered values
    CGFloat filteredX;
    CGFloat filteredY;
    CGFloat filteredZ;
}

@end

@implementation MenuScene

- (id)init
{
    if (self = [super init]) {
        col::DopTree::init();
        
        // Initialize the collision detection pipeline with a given name and id
        _pipeline = new col::CollisionPipeline ("CubeInABoxCollisionThread", 1);
        _pipeline->M_PipelineAlgorithm = col::ALGO_BOXTREE;
        _pipeline->setUseHulls(false);
        _pipeline->verbose(false, false);
        
        // Initialize the base effect
        _baseEffect = [[GLKBaseEffect alloc] init];
        
        // Initialize the motion manager
        _motionManager = [[CMMotionManager alloc] init];
        filteredX = 0.0f;
        filteredY = 0.0f;
        filteredZ = 0.0f;
        
        // Get a reference to the collision utilities class
        _colUtils = [ColUtilsGLES sharedUtils];
        
        // Create the audio player and start playing the background audio
        _audioController = [AudioController sharedController];
        [_audioController createBufferForFileWithFilename:@"background" ofType:@"caf" looping:YES];
        [_audioController playSound:@"background" looping:YES];
        
        // Set the matrices
        _modelviewMatrix = GLKMatrix4Identity;
        _projectionMatrix = GLKMatrix4Identity;
        _screenSize = [[UIScreen mainScreen] bounds].size;
        
        [self setupGL];
        [self setupCollidableObjects];
    }
    return self;
}

- (void)dealloc
{
    delete _pipeline;
}

- (void)setupGL
{
    // Set the projection matrix
    float aspect = fabsf(_screenSize.height / _screenSize.width);
    _projectionMatrix = GLKMatrix4MakePerspective(FovyRadians, aspect, ZNear, ZFar);
    _baseEffect.transform.projectionMatrix = _projectionMatrix;
    
    // Set the accelerometer matrix
    _accelerometerMatrix = GLKMatrix4Identity;
    
    // Adjust the lights
    _baseEffect.lightingType = GLKLightingTypePerPixel;
    _baseEffect.colorMaterialEnabled = GL_TRUE;
    _baseEffect.light0.enabled = GL_TRUE;
    _baseEffect.light0.position = LightPosition1;
}

- (void)setupCollidableObjects
{
    _sceneCage = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_sceneCage setCustomModelGeometryOfType:ColGeometryTypeCage cachedFromObject:nil];
    _sceneCage.currentMatrix = GLKMatrix4MakeScale(2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f));
    _sceneCage.diffuseColor = GLKVector4Make(0.15f, 0.15f, 0.15f, 1.0f);
    _sceneCage.ambientColor = GLKVector4Make(0.2f, 0.4f, 0.2, 1.0f);
    _sceneCage.specularColor = GLKVector4Make(0.1f, 0.1f, 0.1f, 1.0f);
    
    _ballObject = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_ballObject setCustomModelGeometryOfType:ColGeometryTypeBall cachedFromObject:nil];
    _ballObject.currentMatrix = GLKMatrix4MakeTranslation(1.5f, 1.25f, 0.5f);
    _ballObject.diffuseColor = GLKVector4Make(0.75f, 0.25f, 0.15f, 1.0f);
    _ballObject.ambientColor = GLKVector4Make(0.45f, 0.25f, 0.15, 1.0f);
    _ballObject.specularColor = GLKVector4Make(0.1f, 0.1f, 0.1f, 1.0f);
    
    _cubeObject = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_cubeObject setCustomModelGeometryOfType:ColGeometryTypeCrate cachedFromObject:nil];
    _cubeObject.currentMatrix = GLKMatrix4MakeTranslation(-1.75f, -0.75f, -1.0f);
    _cubeObject.diffuseColor = GLKVector4Make(0.0f, 0.5f, 1.0f, 1.0f);
    _cubeObject.ambientColor = GLKVector4Make(0.25f, 0.25f, 0.15, 1.0f);
    _cubeObject.specularColor = GLKVector4Make(0.1f, 0.1f, 0.1f, 1.0f);
    
    _tableObject = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_tableObject setCustomModelGeometryOfType:ColGeometryTypeBarChair cachedFromObject:nil];
    _tableObject.currentMatrix = GLKMatrix4MakeTranslation(2.5f, -1.5f, -1.0f);
    _tableObject.diffuseColor = GLKVector4Make(1.0f, 1.0f, 0.1f, 1.0f);
    _tableObject.ambientColor = GLKVector4Make(0.25f, 0.25f, 0.15, 1.0f);
    _tableObject.specularColor = GLKVector4Make(0.1f, 0.1f, 0.1f, 1.0f);
}

- (void)update
{
    
}

- (void)draw
{
    // Draw the cage
    _modelviewMatrix = GLKMatrix4Multiply(LookAtMenu, TranslationMenu);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _accelerometerMatrix);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _sceneCage.currentMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    _baseEffect.light0.diffuseColor = _sceneCage.diffuseColor;
    _baseEffect.light0.specularColor = _sceneCage.specularColor;
    _baseEffect.light0.ambientColor = _sceneCage.ambientColor;
    [_sceneCage drawWithMode:GL_TRIANGLES];
    
    // Draw the ball
    _modelviewMatrix = GLKMatrix4Multiply(LookAtMenu, TranslationMenu);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _accelerometerMatrix);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _ballObject.currentMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    _baseEffect.light0.diffuseColor = _ballObject.diffuseColor;
    _baseEffect.light0.specularColor = _ballObject.specularColor;
    _baseEffect.light0.ambientColor = _ballObject.ambientColor;
    [_ballObject drawWithMode:GL_TRIANGLES];
    
    // Draw the cube
    _modelviewMatrix = GLKMatrix4Multiply(LookAtMenu, TranslationMenu);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _accelerometerMatrix);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _cubeObject.currentMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    _baseEffect.light0.diffuseColor = _cubeObject.diffuseColor;
    _baseEffect.light0.specularColor = _cubeObject.specularColor;
    _baseEffect.light0.ambientColor = _cubeObject.ambientColor;
    [_cubeObject drawWithMode:GL_TRIANGLES];
    
    // Draw the table
    _modelviewMatrix = GLKMatrix4Multiply(LookAtMenu, TranslationMenu);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _accelerometerMatrix);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _tableObject.currentMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    _baseEffect.light0.diffuseColor = _tableObject.diffuseColor;
    _baseEffect.light0.specularColor = _tableObject.specularColor;
    _baseEffect.light0.ambientColor = _tableObject.ambientColor;
    [_tableObject drawWithMode:GL_TRIANGLES];
}

- (void)startMotionUpdates
{
    [_motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
                                         withHandler:^(CMAccelerometerData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            //Use a basic low-pass filter to only keep the gravity in the accelerometer values
                            filteredY = data.acceleration.x * AccelerationFiltering + filteredY * (1.0 - AccelerationFiltering);
                            filteredX = data.acceleration.y * AccelerationFiltering + filteredX * (1.0 - AccelerationFiltering);
                            filteredZ = data.acceleration.z * AccelerationFiltering + filteredZ * (1.0 - AccelerationFiltering);
                            
                            // Create a vector from the data by reversing the x and y coordinates according to our device orientation
                            GLKVector3 acceleration = GLKVector3Make(-filteredX, filteredY, filteredZ);
                            acceleration = GLKVector3Normalize(acceleration);
                            
                            // Get a vector as a reference
                            GLKVector3 reference = GLKVector3Make(0.0f, 0.0f, 1.0f);
                            GLKVector3 axis = GLKVector3CrossProduct(acceleration, reference);
                            CGFloat angle = acos(GLKVector3DotProduct(axis, reference));
                            
                            // Create a rotation matrix from the quaternion created by the angle / axis of rotation
                            GLKQuaternion quaternion = GLKQuaternionMakeWithAngleAndVector3Axis(angle, axis);
                            _accelerometerMatrix = GLKMatrix4MakeWithQuaternion(quaternion);
                        });
     }];
}

- (void)stopMotionUpdates
{
    [_motionManager stopAccelerometerUpdates];
}

@end
