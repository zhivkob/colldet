//
//  SecondDemoScene.mm
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import "SecondDemoScene.h"

static unsigned int framesCount = 0;

@interface SecondDemoScene()
{
    // Objects that we will need for the collision detection
    col::CollisionPipeline *_pipeline;
    ColUtilsGLES *_colUtils;
    
    NSMutableArray *_collidableObjects;
    CollidableObject *_collidableCage;
    
    // Base effect (mimics shaders)
    GLKBaseEffect *_baseEffect;
    
    GLKMatrix4 _modelviewMatrix;
    GLKMatrix4 _projectionMatrix;
    CGSize _screenSize;
    
    // Shared instance of the audio controller for playing audio effects
    AudioController *_audioController;
}

@end

@implementation SecondDemoScene

- (id)init
{
    if (self = [super init]) {
        _collidableObjects = [NSMutableArray arrayWithCapacity:ObjectsCount];
        
        col::DopTree::init();
        
        // Initialize the collision detection pipeline with a given name and id
        _pipeline = new col::CollisionPipeline ("ObjectsInABoxCollisionThread", 2);
        _pipeline->M_PipelineAlgorithm = col::ALGO_BOXTREE;
        _pipeline->setUseHulls(false);
        _pipeline->verbose(false, false);
        
        // Initialize the base effect
        _baseEffect = [[GLKBaseEffect alloc] init];
        
        // Get a reference to the collision utilities class
        _colUtils = [ColUtilsGLES sharedUtils];
        
        // Set the matrices
        _baseTransformation = GLKMatrix4Identity;
        _modelviewMatrix = GLKMatrix4Identity;
        _projectionMatrix = GLKMatrix4Identity;
        _screenSize = [[UIScreen mainScreen] bounds].size;
        
        // Get the shared instance and buffer the sounds
        _audioController = [AudioController sharedController];
        [_audioController createBufferForFileWithFilename:@"wood_1" ofType:@"caf" looping:NO];
        [_audioController createBufferForFileWithFilename:@"wood_2" ofType:@"caf" looping:NO];
        [_audioController createBufferForFileWithFilename:@"wood_3" ofType:@"caf" looping:NO];
        
        [self setupCollidableObjects];
        [self setupGL];
    }
    return self;
}

- (void)setupGL
{
    _baseEffect.texture2d0.enabled = GL_TRUE;
    _baseEffect.texture2d0.envMode = GLKTextureEnvModeModulate;
    _baseEffect.lightingType = GLKLightingTypePerPixel;
    _baseEffect.colorMaterialEnabled = GL_TRUE;
    _baseEffect.light0.enabled = GL_TRUE;
    _baseEffect.light0.diffuseColor = GLKVector4Make(0.1f, 0.1f, 0.3f, 0.0f);
    _baseEffect.light0.specularColor = GLKVector4Make(0.2f, 0.2f, 0.4f, 0.0f);
    _baseEffect.light0.ambientColor = GLKVector4Make(0.1f, 0.1f, 0.3f, 0.0f);
    _baseEffect.light0.position = LightPosition1;
    
    float aspect = fabsf(_screenSize.height / _screenSize.width);
    _projectionMatrix = GLKMatrix4MakePerspective(FovyRadians, aspect, ZNear, ZFar);
    _baseEffect.transform.projectionMatrix = _projectionMatrix;
}

- (void)dealloc
{
    delete _pipeline;
}

- (void)setupCollidableObjects
{
    _collidableCage = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
    [_collidableCage setCustomModelGeometryOfType:ColGeometryTypeCage cachedFromObject:nil];
    [_collidableCage setTextureWithFilename:@"ConcreteWall" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:nil];
    _collidableCage.currentMatrix = GLKMatrix4MakeScale(2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f), 2.0f * (CageRadius + 1.5f));

    unsigned int objectType = 0;
    
    //
    // Create the collidable objects and whenever possible use the geometry and the texture of a previously created one.
    //
    for (unsigned int i = 0; i < ObjectsCount; i++)
	{
		objectType = i % ColGeometryTypesCount;

        CollidableObject *object = [[CollidableObject alloc] initWithPipeline:_pipeline baseEffect:_baseEffect];
		switch (objectType)
		{
			case ColGeometryTypeBall:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeBall];
                [object setCustomModelGeometryOfType:ColGeometryTypeBall cachedFromObject:similarObject];
				break;
			}
			case ColGeometryTypeVase:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeVase];
                [object setCustomModelGeometryOfType:ColGeometryTypeVase cachedFromObject:similarObject];
                [object setTextureWithFilename:@"Vase" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:similarObject];
				break;
			}
			case ColGeometryTypeTable:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeTable];
                [object setCustomModelGeometryOfType:ColGeometryTypeTable cachedFromObject:similarObject];
                [object setTextureWithFilename:@"Table" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:similarObject];
				break;
			}
			case ColGeometryTypeCrate:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeCrate];
                [object setCustomModelGeometryOfType:ColGeometryTypeCrate cachedFromObject:similarObject];
                [object setTextureWithFilename:@"Crate" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:similarObject];
				break;
			}
            case ColGeometryTypeCage:
			case ColGeometryTypeMushroom:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeMushroom];
                [object setCustomModelGeometryOfType:ColGeometryTypeMushroom cachedFromObject:similarObject];
                [object setTextureWithFilename:@"Mushroom" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:similarObject];
				break;
			}
			case ColGeometryTypeOther:
			case ColGeometryTypeBarChair:
			{
                CollidableObject *similarObject = [self similarObjectToObjectWithGeometryType:ColGeometryTypeBarChair];
                [object setCustomModelGeometryOfType:ColGeometryTypeBarChair cachedFromObject:similarObject];
                [object setTextureWithFilename:@"Table" ofType:ColTextureTypePNG options:nil asynchronously:NO cachedFromObject:similarObject];
				break;
			}
		}
        
        [object setRandomVelocityInBoxWithWidth:CageRadius * 0.8f];
        [object setRandomPositionInBoxWithWidth:CageRadius * 0.8f];
        [object makeCollidable];
        [_collidableObjects addObject:object];
	}
    
    // Register callbacks for each pair of objects
    // Here you can specify which callback class should be used
    for (unsigned int i = 0; i < ObjectsCount; i++)
        for (unsigned int j = 0; j < i; j++) {
            CollisionCallback *collisionCallback = new CollisionCallback([_collidableObjects objectAtIndex:j], [_collidableObjects objectAtIndex:i], LevelOfDetection);
            collisionCallback->collisionBlock = ^(CollidableObject *collObject1, CollidableObject *collObject2) {
                // If one of both objects is already colliding, then skip the procedure
                // Also if a collision happened no so long ago, don't proceed, because we might be stuck forever
                //
                
                BOOL lastCollision = fabsf(collObject1.hitFrame - framesCount) < LastCollisionFramesCountThreshold;
                if (!collObject1.isColliding && !collObject2.isColliding && !lastCollision) {
                    collObject1.isColliding = YES;
                    collObject2.isColliding = YES;
                    
                    int random = arc4random() % 4 + 1;
                    NSString *sound = [NSString stringWithFormat:@"wood_%d", random];
                    [_audioController playSound:sound looping:NO];
                    
                    // Calculate the collision normal for both objects
                    GLKVector3 object1Position = GLKVector3Make(collObject1.currentMatrix.m30, collObject1.currentMatrix.m31, collObject1.currentMatrix.m32);
                    GLKVector3 object2Position = GLKVector3Make(collObject2.currentMatrix.m30, collObject2.currentMatrix.m31, collObject2.currentMatrix.m32);
                    GLKVector3 normal = GLKVector3Normalize(GLKVector3Subtract(object1Position, object2Position));
                    [collObject1 setCollisionNormal:GLKVector3Negate(normal) forHitFrame:framesCount];
                    [collObject1 invertVelocity];
                    [collObject2 setCollisionNormal:normal forHitFrame:framesCount];
                    [collObject2 invertVelocity];
                }
            };
            _pipeline->addCallback(collisionCallback);
        }
}

// Returns an object with the same geometry if one exists
- (CollidableObject *)similarObjectToObjectWithGeometryType:(ColGeometryType)geometryType
{
    for (CollidableObject *object in _collidableObjects) {
        if (geometryType == object.geometryType)
            return object;
    }
    return nil;
}

- (void)update
{
    // Update the position of the object
    for (CollidableObject *collObject in _collidableObjects) {
        // Save the current matrix as an old one
        collObject.oldMatrix = collObject.currentMatrix;
        
        // Rotate and translate the object
        GLKMatrix4 rotation = GLKMatrix4RotationFromMatrix(collObject.currentMatrix);
        rotation = GLKMatrix4Multiply(rotation, collObject.rotVelocity);
        GLKMatrix4 translation = GLKMatrix4TranslateFromMatrix(collObject.currentMatrix);
        translation = GLKMatrix4TranslateWithVector3(translation, collObject.translVelocity);
        collObject.currentMatrix = GLKMatrix4Multiply(translation, rotation);
        GLKVector3 colNormalWall = GLKVector3Make(0.0f, 0.0f, 0.0f);
        
        if (collObject.currentMatrix.m30 > CageRadius)
            colNormalWall.x = -1.0f;
        else
            if (collObject.currentMatrix.m30 < -CageRadius)
                colNormalWall.x = 1.0f;
        
        if (collObject.currentMatrix.m31 > CageRadius)
            colNormalWall.y = -1.0f;
        else
            if (collObject.currentMatrix.m31 < -CageRadius)
                colNormalWall.y = 1.0f;
        
        if (collObject.currentMatrix.m32 > CageRadius)
            colNormalWall.z = -1.0f;
        else
            if (collObject.currentMatrix.m32 < -CageRadius)
                colNormalWall.z = 1.0f;
        
        if (col::NearZero < GLKVector3Length(colNormalWall))
        {
            colNormalWall = GLKVector3Normalize(colNormalWall);
            [collObject setCollisionNormal:colNormalWall forHitFrame:framesCount];
            [collObject invertVelocity];
            collObject.isColliding = NO;
        }
        
        // Clear the collision flag if we have passed the threshold of N frames after the last collision
        // This way we are avoiding in a very simple way some of the situation we might get stuck. You would
        // want to check that case better in your application!!!
        if ((fabsf(collObject.hitFrame - framesCount) > LastCollisionFramesCountThreshold) && collObject.isColliding)
            collObject.isColliding = NO;
        
        [collObject updatePipeline];
    }
    
    framesCount++;
    _pipeline->check();
}

- (void)draw
{
    for (CollidableObject *collObject in _collidableObjects) {
        _modelviewMatrix = GLKMatrix4Multiply(LookAtSecondDemo, TranslationSecondDemo);
        _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _baseTransformation);
        _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, collObject.currentMatrix);
        _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
        
        if (collObject.textureInfo) {
            _baseEffect.texture2d0.enabled = GL_TRUE;
            _baseEffect.texture2d0.name = collObject.textureInfo.name;
        }
        else
            _baseEffect.texture2d0.enabled = GL_FALSE;
        _baseEffect.material.diffuseColor = collObject.diffuseColor;
        _baseEffect.material.specularColor = collObject.specularColor;
        _baseEffect.material.ambientColor = collObject.ambientColor;
        _baseEffect.material.shininess = collObject.shininess;     
        [collObject drawWithMode:GL_TRIANGLES];
    }
    
    _modelviewMatrix = GLKMatrix4Multiply(LookAtSecondDemo, TranslationSecondDemo);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _baseTransformation);
    _modelviewMatrix = GLKMatrix4Multiply(_modelviewMatrix, _collidableCage.currentMatrix);
    _baseEffect.transform.modelviewMatrix = _modelviewMatrix;
    
    if (_collidableCage.textureInfo) {
        _baseEffect.texture2d0.enabled = GL_TRUE;
        _baseEffect.texture2d0.name = _collidableCage.textureInfo.name;
    }
    else
        _baseEffect.texture2d0.enabled = GL_FALSE;
    [_collidableCage drawWithMode:GL_TRIANGLES];
}

@end
