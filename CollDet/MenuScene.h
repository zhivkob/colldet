//
//  MenuScene.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import "Scene.h"
#import "Constants.h"

@interface MenuScene : NSObject <Scene>

- (void)startMotionUpdates;
- (void)stopMotionUpdates;

@end
