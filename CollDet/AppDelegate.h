//
//  AppDelegate.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/13/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
