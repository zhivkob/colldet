//
//  Constants.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/6/12.
//
//

#ifndef COLLDET_CONSTANTS_H
#define COLLDET_CONSTANTS_H

#import <GLKit/GLKit.h>
#import <Collision.h>
#import <ColUtils.h>

#define RADIANS_PER_PIXEL (M_PI / 320.f)

/**
 *
 *  Constants we use in the main scene
 *
 */

FOUNDATION_EXPORT CGFloat const AccelerationFiltering;

FOUNDATION_EXPORT GLKVector3 const EyePositionMenu;
FOUNDATION_EXPORT GLKMatrix4 TranslationMenu;
FOUNDATION_EXPORT GLKMatrix4 LookAtMenu;

/**
 *
 *  Constants we use in the first demo
 *
 */

FOUNDATION_EXPORT CGFloat const SpaceBetweenBalls;
FOUNDATION_EXPORT NSUInteger const BallsCount;
FOUNDATION_EXPORT CGFloat const BoundsOffset;
FOUNDATION_EXPORT CGFloat const RightBound;
FOUNDATION_EXPORT CGFloat const LeftBound;
FOUNDATION_EXPORT CGFloat const BallRadius;
FOUNDATION_EXPORT NSUInteger const BallDepth;

FOUNDATION_EXPORT GLKVector3 const RotationAxis;
FOUNDATION_EXPORT CGFloat const RotationFriction;
FOUNDATION_EXPORT CGFloat const RotationAngle;
FOUNDATION_EXPORT CGFloat const MovementOffset;
FOUNDATION_EXPORT CGFloat const SceneRotationAngle;

FOUNDATION_EXPORT CGFloat const PlaneWidth;
FOUNDATION_EXPORT CGFloat const PlaneHeight;

FOUNDATION_EXPORT BOOL const BallsRotate;

FOUNDATION_EXPORT GLKVector3 const EyePosition0;

FOUNDATION_EXPORT GLKMatrix4 TranslationFirstDemo;
FOUNDATION_EXPORT GLKMatrix4 LookAtFirstDemo;

/**
 *
 *  Constants we use in the second demo
 *
 */

FOUNDATION_EXPORT CGFloat const CageRadius;
FOUNDATION_EXPORT NSUInteger const ObjectsCount;
FOUNDATION_EXPORT NSUInteger const Complexity;
FOUNDATION_EXPORT CGFloat const ObjectsRadius;

FOUNDATION_EXPORT CGFloat const InterpolationAlpha;

FOUNDATION_EXPORT GLKVector4 const LightPosition1;
FOUNDATION_EXPORT GLKVector3 const EyePosition1;

FOUNDATION_EXPORT GLKMatrix4 TranslationSecondDemo;
FOUNDATION_EXPORT GLKMatrix4 LookAtSecondDemo;

/**
 *
 *  Constants we use in the entire application
 *
 */

FOUNDATION_EXPORT col::LevelOfDetectionE const LevelOfDetection;
FOUNDATION_EXPORT unsigned int const LastCollisionFramesCountThreshold;

FOUNDATION_EXPORT GLenum const discards[];

FOUNDATION_EXPORT float const FovyRadians;
FOUNDATION_EXPORT float const ZNear;
FOUNDATION_EXPORT float const ZFar;

#endif