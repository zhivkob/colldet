
//***************************************************************************
//                              ColConvexHull
//***************************************************************************
//  Copyright (C):
//***************************************************************************
//CVSId: "@(#)$Id: ColConvexHull.h,v 1.3 2004/02/26 14:50:15 ehlgen Exp $"
//***************************************************************************


#ifndef ColConvexHull_H
#define ColConvexHull_H
#if defined(__sgi) || defined(_WIN32)
#pragma once
#endif

//---------------------------------------------------------------------------
//  Includes
//---------------------------------------------------------------------------
#include <cfloat>

#include <col_import_export.h>
#include <ColGeometry.h>
#include <ColTopology.h>
#include <ColUtils.h>

namespace col {


//---------------------------------------------------------------------------
//  Forward References
//---------------------------------------------------------------------------

struct SepPlane;

//---------------------------------------------------------------------------
//   Constants
//---------------------------------------------------------------------------

/** Some constants for the separating planes algo ConvexHull::check();
 *  optimal values determined by experiments.
 */
const REAL M_InitEta = 0.1;
const REAL M_MaxSteps = 150;
const REAL M_AnnealingFactor = 0.97;


//***************************************************************************
//  ColConvexHull
//***************************************************************************


class COL_EXPORTIMPORT ConvexHull
{


//---------------------------------------------------------------------------
//  Public Instance methods
//---------------------------------------------------------------------------

public:

	ConvexHull( );
	explicit ConvexHull( const ConvexHull & source );
	void operator = ( const ConvexHull & source );

	ConvexHull( const ColGeometry * geom );
	void operator = ( const ColGeometry * geom );

	bool check( const ConvexHull &other, const Matrix4 & m12,
				SepPlane *plane ) const;

	int buildGeometry( ColGeometry * geo );
	void print( void );

	virtual ~ConvexHull() throw();

//---------------------------------------------------------------------------
//  Instance variables
//---------------------------------------------------------------------------

protected:

	/// vertices of the hull; this is a subset of the points of @a org_geom
	Pnt3Array m_vertex;

	/// faces of the hull, contains indices into @a vertex
	vector<TopoFace> m_face;

	/// the original geometry of which @a self will be the convex hull
	const ColGeometry * m_org_geom;		// TODO GeometryConstPtr, wenn OSG soweit

	/** The topology of the convex hull (incidence and adjacency realtions);
	 *  needed for the intersection test of two convex hulls.
	 */
	Topology m_topo;

//---------------------------------------------------------------------------
//  Class variables
//---------------------------------------------------------------------------
	
//---------------------------------------------------------------------------
//  Private Instance methods
//---------------------------------------------------------------------------
	
protected:

	void createHull( void );

};


//***************************************************************************
//  SepPlane
//***************************************************************************

struct COL_EXPORTIMPORT SepPlane
{
	SepPlane();

	Vector4 m_w;
	unsigned int m_closest_p1, m_closest_p2;

};


} // namespace col

#endif /* ConvexHull_H */

