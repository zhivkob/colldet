//
//  AudioController.m
//  AudioController
//
//  Created by Zhivko Bogdanov on 7/7/12.
//  Copyright (c) 2012 Zhivko Bogdanov. All rights reserved.
//

#import "AudioController.h"

@implementation AudioController

static AudioController *sharedController = nil;

/** @name Initialization */

/**
 *  A designated initializer.
 *
 */

- (id)init
{
    if (self = [super init]) {
        _device = alcOpenDevice(NULL);
        
        if (_device) {
            _context = alcCreateContext(_device, NULL);
            alcMakeContextCurrent(_context);
        }
        
        // Dictionaries to store all the buffers and sounds
        buffers = [[NSMutableDictionary alloc] init];
        sounds = [[NSMutableDictionary alloc] init];
    }
    return self;
}

/**
 *  We override this method to avoid thread issues with the singleton instance of the class.
 *
 */

+ (void)initialize
{
    if (self == [AudioController class]) {
        sharedController = [[AudioController alloc] init];
    }
}

/**
 *  Get a reference to the shared instance of the audio controller.
 *  @return An allocated and initialized instance _sharedController_ of the class.
 *
 */

+ (id)sharedController
{
    return sharedController;
}

/** @name Audio file creation */

/**
 *
 *  This method opens an audio file and creates an audio file id which will be used later when we read the data from the file.
 *  This method will be used in the more general method createBufferForFileWithFilename:ofType:looping:
 *
 *  @param path The path to the file specified as NSString.
 *
 *  @return Returns a generated audio file ID.
 */

- (AudioFileID)openAudioFile:(NSString *)path
{
    AudioFileID audioFileID;
    NSURL *filepath = [NSURL fileURLWithPath:path];
    
#if TARGET_OS_IPHONE
	OSStatus result = AudioFileOpenURL((__bridge_retained CFURLRef)filepath, kAudioFileReadPermission, 0, &audioFileID);
#else
	OSStatus result = AudioFileOpenURL((__bridge_retained CFURLRef)filepath, fsRdPerm, 0, &audioFileID);
#endif
	if (result)
        NSLog(@"Could not open file %@.", filepath);
    
	return audioFileID;
}

/**
 *
 *  A method used to calculate the size of a given audio file.
 *
 *  @param fileDescriptor The ID we have generated for the audio file.
 *
 *  @return Returns a 32-bit file size.
 */

- (UInt32)audioFileSize:(AudioFileID)fileDescriptor
{
    UInt64 outDataSize = 0;
	UInt32 thePropSize = sizeof(UInt64);
    
	OSStatus result = AudioFileGetProperty(fileDescriptor, kAudioFilePropertyAudioDataByteCount, &thePropSize, &outDataSize);
	if (result)
        NSLog(@"Could not find the file size.");
    
	return (UInt32)outDataSize;
}

/**
 *
 *  Create an openAL audio buffer for a given audio file of type .caf (hardcoded) with frequency 44 100 Hz.
 *
 *  @param filename The name of the audio file.
 *  @param type The type of the audio file (in our case .caf).
 *  @param loop Indicates if the buffer should be created with a looping audio file.
 */

- (void)createBufferForFileWithFilename:(NSString *)filename ofType:(NSString *)type looping:(BOOL)loop
{
    ALenum  error = AL_NO_ERROR;
    ALuint bufferID;
    ALuint sourceID;
	ALenum  format;
	ALsizei size;
	ALsizei freq;
    CFURLRef fileURL = (__bridge CFURLRef)[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:filename ofType:type]];
    void *data = MyGetOpenALAudioData(fileURL, &size, &format, &freq);
    
    if((error = alGetError()) != AL_NO_ERROR) {
        NSLog(@"Error loading sound: %x\n", error);
        return;
    }
    
    alGenBuffers(1, &bufferID);
    alBufferData(bufferID, format, data, size, freq);
    
    // Save the buffer in an array
    [buffers setObject:@(bufferID) forKey:filename];
    
    // Generate a source id and attach the buffer to the source
    alGenSources(1, &sourceID);
    alSourcei(sourceID, AL_BUFFER, bufferID);
    
    // Store the sound source id we've generated to use the sound later again
    [sounds setObject:@(sourceID) forKey:filename];
    
    // Free the memory we allocated
    if (data)
    {
        free(data);
        data = NULL;
    }
}

/**
 *
 *  A convenience method we use in our playSound:looping: method.
 *  It looks for a buffer that is currently not being used. If there isn't any, we look for a looping one, stop it and play this one.
 *  If all the sources are occupied at the moment we take the first source and we play the sound using it.
 *
 *  @return Returns the source ID of the next available source.
 */

- (ALuint)nextAvailableSource
{
    // Save the current state of a given source here
	ALint sourceState;
	
	// Check if a source is not being used at this moment
	for (NSNumber *sourceNumber in [sounds allValues]) {
        ALuint sourceID = (ALuint)[sourceNumber unsignedIntegerValue];
		alGetSourcei(sourceID, AL_SOURCE_STATE, &sourceState);
        
        // Found one
		if (sourceState != AL_PLAYING)
            return sourceID;
	}
	
	// All our sources are used at that moment. Stop a not looping one and play this one.
	ALint loop;
    
	for (NSNumber *sourceNumber in [sounds allValues]) {
        ALuint sourceID = (ALuint)[sourceNumber unsignedIntegerValue];
		alGetSourcei(sourceID, AL_LOOPING, &loop);
        
        // Found one that is not looping. Stop it and return the source id.
		if (!loop) {
			alSourceStop(sourceID);
			return sourceID;
		}
	}
	
	// All our sources are used at that moment. Stop the first one one and play this one.	
	ALuint sourceID = (ALuint)[[[sounds allValues] objectAtIndex:0] unsignedIntegerValue];
	alSourceStop(sourceID);
    
	return sourceID;
}

/** @name Controling audio files */

/**
 *
 *  A method we use to play a sound with a given name, saved in our dictionary of sounds and sources.
 *  We can decide if the sound is looping or not.
 *  For the sound effect should be created a buffer before trying to play anything!
 *
 *  @param soundName Contains the name of the sound that we want to play.
 *  @param loop When set to _YES_ plays the sound indefinetely.
 */

- (void)playSound:(NSString*)soundName looping:(BOOL)loop
{
	ALenum err = alGetError();
	
	// Find the buffer we would like to play
	NSNumber *value = [sounds objectForKey:soundName];
	if (value == nil)
        return;
    
    // Get the buffer id
	ALuint bufferID = (ALuint)[[buffers objectForKey:soundName] unsignedIntegerValue];
	
	// Find an available source
	ALuint sourceID = [self nextAvailableSource];

	// Attach the source to the buffer
	alSourcei(sourceID, AL_BUFFER, bufferID);
	
	// Set the pitch and the gain
	alSourcef(sourceID, AL_PITCH, 1.0f);
	alSourcef(sourceID, AL_GAIN, 1.0f);
	
	// Set if the sound should loop or not
	if (loop) {
		alSourcei(sourceID, AL_LOOPING, AL_TRUE);
	} else {
		alSourcei(sourceID, AL_LOOPING, AL_FALSE);
	}
    
	// Check for errors
	err = alGetError();
	if (err) {
        NSLog(@"Error encountered.");
        return;
	}
    
    // Play the sound
	alSourcePlay(sourceID);
}

/**
 *
 *  A method we use to stop a sound with a given name, saved in our dictionary of sounds and sources.
 *
 *  @param soundName Contains the name of the sound that we want to stop.
 */

- (void)stopSound:(NSString*)soundName
{ 
	NSNumber *value = [sounds objectForKey:soundName];
	if (value == nil)
        return;
    
	ALuint sourceID = (ALuint)[value unsignedIntegerValue];	
	alSourceStop(sourceID);	
}

/** @name Configuring the source and listener parameters */

/**
 *  Set the listener's position.
 *
 *  @param position A 3D-array containing (x, y, z) coordinates to describe the position of the listener.
 */

- (void)setListenerPosition:(ALfloat *)position
{
    alListenerfv(AL_POSITION, position);
}

/**
 *  Set the listener's velocity.
 *
 *  @param velocity A 3D-array containing (x, y, z) coordinates to describe the velocity of the listener.
 */

- (void)setListenerVelocity:(ALfloat *)velocity
{
    alListenerfv(AL_VELOCITY, velocity);
}

/**
 *  Set the listener's orientation
 *
 *  @param orientation A 3D-array containing (x, y, z) coordinates to describe the orientation of the listener.
 */

- (void)setListenerOrientation:(ALfloat *)orientation
{
    alListenerfv(AL_ORIENTATION, orientation);
}

/**
 *  Set the source position.
 *
 *  @param position A 3D-array containing (x, y, z) coordinates to describe the position of the source.
 *  @param soundName The name of the sound we would like to change the property of.
 */

- (void)setSourcePosition:(ALfloat *)position withSourceName:(NSString *)soundName
{
    ALuint sourceID = (ALuint)[[sounds objectForKey:soundName] unsignedIntegerValue];
    alSourcefv(sourceID, AL_POSITION, position);
}

/**
 *  Set the source velocity.
 *
 *  @param velocity A 3D-array containing (x, y, z) coordinates to describe the velocity of the source.
 *  @param soundName The name of the sound we would like to change the property of.
 */

- (void)setSourceVelocity:(ALfloat *)velocity withSourceName:(NSString *)soundName
{
    ALuint sourceID = (ALuint)[[sounds objectForKey:soundName] unsignedIntegerValue];
    alSourcefv(sourceID, AL_POSITION, velocity);
}

/**
 *
 *  A method used to delete all buffers, destroy the current context and close the device.
 */

- (void)cleanOpenAL
{
	// Delete all sources
	for (NSNumber *sourceNumber in [sounds allValues]) {
		ALuint sourceID = (ALuint)[sourceNumber unsignedIntegerValue];
		alDeleteSources(1, &sourceID);
	}
	
	// Delete all buffers
	for (NSNumber *bufferNumber in buffers) {
		ALuint bufferID = (ALuint)[bufferNumber unsignedIntegerValue];
		alDeleteBuffers(1, &bufferID);
	}
	
	// Destroy the current context
	alcDestroyContext(_context);
    
	// Close the device
	alcCloseDevice(_device);
}

@end
