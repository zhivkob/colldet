
//***************************************************************************
//                             config.h 
//***************************************************************************

#ifndef Config_H
#define Config_H

#if defined(__sgi) || defined(_WIN32)
#pragma once
#endif

/** the version of current CollDet library
 */
#define COLLDET_VERSION 

/** the float type which CollDet is using now
 */
typedef double REAL;

/** the path where data should be loaded
 */
#define FILE_LOAD_PATH " "

#endif //Config_h
