//
//  AudioController.h
//  AudioController
//
//  Created by Zhivko Bogdanov on 7/7/12.
//  Copyright (c) 2012 Zhivko Bogdanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>
#import "MyOpenALSupport.h"

@interface AudioController : NSObject
{
    ALCcontext *_context;
    ALCdevice *_device;
    
    // Dictionaries to save the buffers and the sound names with the corresponding sources
    NSMutableDictionary *buffers;
    NSMutableDictionary *sounds;
}

+ (id)sharedController;

// Create a buffer id for our sound and link it to a source id
- (void)createBufferForFileWithFilename:(NSString *)filename ofType:(NSString *)type looping:(BOOL)loop;

// Play or stop a sound with a given name
- (void)playSound:(NSString*)soundName looping:(BOOL)loop;
- (void)stopSound:(NSString*)soundName;

// Adjust listener's position, velocity and orientation
- (void)setListenerPosition:(ALfloat *)position;
- (void)setListenerVelocity:(ALfloat *)velocity;
- (void)setListenerOrientation:(ALfloat *)orientation;

// Adjust source's position and velocity
- (void)setSourcePosition:(ALfloat *)position withSourceName:(NSString *)soundName;
- (void)setSourceVelocity:(ALfloat *)velocity withSourceName:(NSString *)soundName;

// Deallocate all openAL variables we were using
- (void)cleanOpenAL;

@end
