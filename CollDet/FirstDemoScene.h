//
//  FirstDemoScene.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import "Scene.h"

@interface FirstDemoScene : NSObject <Scene>

@property (nonatomic) GLKMatrix4 baseTransformation;

@end
