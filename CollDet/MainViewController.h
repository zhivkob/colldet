//
//  ViewController.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuScene.h"
#import "FirstDemoScene.h"
#import "SecondDemoScene.h"
#import "AudioController.h"

enum Scene {
    SCENE_MENU = 0,
    SCENE_FIRST_DEMO,
    SCENE_SECOND_DEMO
};

@interface MainViewController : GLKViewController <GLKViewDelegate>

@end
