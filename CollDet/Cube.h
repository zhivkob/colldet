//
//  Cube.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <vectormath/scalar/cpp/vectormath_aos.h>

using namespace Vectormath::Aos;

const unsigned int CubePointsCount = 8;
const unsigned int CubeQuadsCount = 6;
const unsigned int CubeTrianglesCount = 12;

/** 
 *  8 Vertices of standard cube
 */
const Point3 CubePoints[CubePointsCount] = { 
    Point3(-1,-1, 1),
    Point3( 1,-1, 1),
    Point3( 1, 1, 1),
    Point3(-1, 1, 1),
    Point3(-1,-1,-1),
    Point3( 1,-1,-1),
    Point3( 1, 1,-1),
    Point3(-1, 1,-1)
};

/**
 *  6 Quads of standard cube
 */
const unsigned int CubeQuadIndices[CubeQuadsCount * 4]={
    0,1,2,3,
    1,5,6,2,
    2,6,7,3,
    0,3,7,4,
    0,4,5,1,
    4,7,6,5
};

/**
 *  12 Triangles of standard cube
 */
const unsigned int CubeTriIndices[CubeTrianglesCount * 3]={
    0,1,2,
    2,3,0,
    1,5,6,
    6,2,1,
    2,6,7,
    7,3,2,
    0,3,7,
    7,4,0,
    0,4,5,
    5,1,0,
    4,7,6,
    6,5,4
};
