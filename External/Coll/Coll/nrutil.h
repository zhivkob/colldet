
#include <ColDefs.h>

namespace nr
{
void nrerror(char error_text[]);
REAL *vector(long nl, long nh);
int *ivector(long nl, long nh);
REAL **matrix(long nrl, long nrh, long ncl, long nch);

void free_vector(REAL *v, long nl, long nh);
void free_ivector(int *v, long nl, long nh);
void free_matrix(REAL **m, long nrl, long nrh, long ncl, long nch);

}

/* (C) Copr. 1986-92 Numerical Recipes Software &(')'$. */
