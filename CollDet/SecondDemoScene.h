//
//  SecondDemoScene.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 7/7/12.
//
//

#import "Scene.h"
#import "Constants.h"

@interface SecondDemoScene : NSObject <Scene>

@property (nonatomic) GLKMatrix4 baseTransformation;

@end
