//
//  ColUtilsGLES.h
//  CollDet
//
//  Created by Zhivko Bogdanov on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import <OpenGLES/ES2/glext.h>
#import <ColGeometry.h>
#import <ColTopology.h>
#import <iostream>
#import <fstream>
#import <vector>
#import "Cube.h"
#import "Icosahedron.h"

#define BUFFER_OFFSET(i) (reinterpret_cast<void*>(i))
#define COL_INLINE static __inline__

// Uniform index.
enum
{
    UNIFORM_EYE_POSITION,
    UNIFORM_DIFFUSE_COLOR,
    UNIFORM_AMBIENT_COLOR,
    UNIFORM_SPECULAR_COLOR,
    UNIFORM_SHININESS,
    UNIFORM_MODELVIEW_MATRIX,
    UNIFORM_PROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    UNIFORM_LIGHT_POSITION,
    UNIFORM_TEXTURE,
    NUM_UNIFORMS
};

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    ATTRIB_TEXTURE,
    NUM_ATTRIBUTES
};

typedef enum {
    ColTextureTypePVR = 0,
    ColTextureTypePNG,
    ColTextureTypeJPEG
} ColTextureType;

typedef enum {
    ColGeometryTypeCrate = 0,
    ColGeometryTypeCage,
    ColGeometryTypeBall,
    ColGeometryTypeBarChair,
    ColGeometryTypeTable,
    ColGeometryTypeVase,
    ColGeometryTypeMushroom,
    ColGeometryTypeOther,
    ColGeometryTypesCount
} ColGeometryType;

//
// Convert a GLKMatrix4 matrix into a col::Matrix4 matrix.
//
COL_INLINE Matrix4 ColMatrix4MakeWithGLKMatrix4(GLKMatrix4 &mat)
{
    return Matrix4(Vector4(mat.m00, mat.m01, mat.m02, mat.m03),
                   Vector4(mat.m10, mat.m11, mat.m12, mat.m13),
                   Vector4(mat.m20, mat.m21, mat.m22, mat.m23),
                   Vector4(mat.m30, mat.m31, mat.m32, mat.m33));
}

COL_INLINE GLKVector3 GLKVector3ScaleFromMatrix4(GLKMatrix4 m)
{
    GLKVector3 v = GLKVector3Make(sqrt(m.m00*m.m00 + m.m01*m.m01 +m.m02*m.m02),
                                  sqrt(m.m10*m.m10 + m.m11*m.m11 +m.m12*m.m12),
                                  sqrt(m.m20*m.m20 + m.m21*m.m21 +m.m22*m.m22));
    
    return v;
}

COL_INLINE GLKMatrix4 GLKMatrix4RotationFromMatrix(GLKMatrix4 m)
{
    GLKVector3 s = GLKVector3ScaleFromMatrix4(m);
    
    GLKMatrix4 n_matrix;
    n_matrix.m00 = m.m00/s.x;
    n_matrix.m01 = m.m01/s.x;
    n_matrix.m02 = m.m02/s.x;
    n_matrix.m03 = 0;
    
    n_matrix.m10 = m.m10/s.y;
    n_matrix.m11 = m.m11/s.y;
    n_matrix.m12 = m.m12/s.y;
    n_matrix.m13 = 0;
    
    n_matrix.m20 = m.m20/s.z;
    n_matrix.m21 = m.m21/s.z;
    n_matrix.m22 = m.m22/s.z;
    n_matrix.m23 = 0;
    
    n_matrix.m30 = 0;
    n_matrix.m31 = 0;
    n_matrix.m32 = 0;
    n_matrix.m33 = 1;
    
    return n_matrix;
}


COL_INLINE GLKVector3 GLKVector3RotationFromMatrix(GLKMatrix4 matrix)
{
    GLKVector3 rotate;
    
    // decompose main matrix and take just rotation matrix
    GLKMatrix4 rMatrix = GLKMatrix4RotationFromMatrix(matrix);
    
    rotate.x = atan2f(rMatrix.m21, rMatrix.m22);
    rotate.y = atan2f(-rMatrix.m20, sqrtf(rMatrix.m21*rMatrix.m21 + rMatrix.m22*rMatrix.m22) );
    rotate.z = atan2f(rMatrix.m10, rMatrix.m00);
    
    return rotate;
}

COL_INLINE GLKVector3 GLKVector3TranslateFromMatrix(GLKMatrix4 matrix)
{
    GLKVector3 translate = GLKVector3Make(matrix.m30, matrix.m31, matrix.m32);
    
    return translate;
}

COL_INLINE GLKMatrix4 GLKMatrix4TranslateFromMatrix(GLKMatrix4 matrix) {
    return GLKMatrix4MakeTranslation(matrix.m30, matrix.m31, matrix.m32);
}

@interface ColUtilsGLES : NSObject
{
    // A GLKTextureLoader instance to load textures.
    GLKTextureLoader *_textureLoader;
}

// Designated initializer
+ (id)sharedUtils;


// Load a custom geometry with a given filename
- (BOOL)loadGeometry:(col::ColGeometry *)geo withGeometryType:(ColGeometryType)geometryType;

// Draw a geometry to a OpenGL scene
- (void)drawGeometry:(col::ColGeometry *)geo withMode:(GLenum)mode geometryType:(ColGeometryType)geometryType;


// Create an OpenGL texture from an image
- (GLKTextureInfo *)textureFromImageWithFilename:(NSString *)filename
                                          ofType:(ColTextureType)textureType
                                         options:(NSDictionary *)options;

// Create an OpenGL texture from an image (asynchronously)
- (void)textureAsynchronouslyFromImageWithFilename:(NSString *)filename
                                            ofType:(ColTextureType)textureType
                                           options:(NSDictionary *)options
                                        completion:(void (^)(GLKTextureInfo *textureInfo, NSError *outError))completionHandler;




// Create a vertex array object, vertex buffer object, index buffer and allocate enough memory for the vertices data and the indices data
// of a given geometry
- (void)createVertexArray:(GLuint& )vao
    forVertexBufferObject:(GLuint& )vbo
              forGeometry:(col::ColGeometry const *)geo
             geometryType:(ColGeometryType)geometryType;

// Free the allocated space for the vertices data and indices data and delete the vertex array object, vertex buffer object, and
// the index buffer
- (void)deleteVertexArray:(GLuint& )vao
    forVertexBufferObject:(GLuint& )vbo;

// Make a sphere for a given geometry
- (void)makeSphere:(col::ColGeometry *)geo
         withDepth:(unsigned int)depth
         andRadius:(REAL)radius;

// Make a cube for a given geometry
- (void)makeCube:(col::ColGeometry *)geo withRadius:(REAL)radius;

// Make a plane for a given geometry
- (void)makePlane:(col::ColGeometry *)geo
        withWidth:(REAL)width
           height:(REAL)height
 atVerticalHeight:(unsigned int)vert
andHorizontalHeight:(unsigned int)hor;

// Generate a random float number between 0 and 1
- (float)randomFloatBetweenZeroAndOne;

// Load a shader pair with a given filename for a given program with dictionaries containing the attributes and the uniforms of these shaders
- (BOOL)loadShadersWithFilename:(NSString *)filename
                     forProgram:(GLuint *)program
                 withAttributes:(NSDictionary *)attributes
                    andUniforms:(NSDictionary *)unifsDict
              withUniformsArray:(GLint *)unifs;

@end
