
#ifndef LULGS_H
#define LULGS_H

#ifdef _SGI_SOURCE
#pragma once
#endif

namespace nr
{

extern void ludcmp(REAL **a, int n, int *indx, REAL *d, int *ludc);

extern void lubksb(REAL **a, int n, int *indx, REAL b[]);

}

#endif


